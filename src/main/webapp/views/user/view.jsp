<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>


<div class="row">
	<div class="col-8">
		<spring:message code="user.folowers" var="suc"/>
		<h3><jstl:out value="${suc}"/></h3>
		<display:table pagesize="15" class="displaytag" keepStatus="true"
					   name="${us.followers}" requestURI="${requestURI}" id="row">

			<spring:message code="user.name" var="name" />
			<display:column property="name" title="${name}" sortable="true" />
			<spring:message code="user.surname" var="surname" />
			<display:column property="surname" title="${surname}" sortable="true" />
			<spring:message code="user.picture" var="picture" />
			<display:column title="${picture}">
				<img src="${row.picture}" width="80" height="80" >
			</display:column>
			<spring:message code="user.address" var="address" />
			<display:column property="address" title="${address}"
							sortable="true" />
			<spring:message code="user.email" var="email" />
			<display:column property="email" title="${email}" sortable="true" />
			<spring:message code="user.phone" var="phone" />
			<display:column property="phone" title="${phone}" sortable="true" />


		</display:table>


		<spring:message code="user.followings" var="suc"/>
		<h3><jstl:out value="${suc}"/></h3>
		<display:table pagesize="15" class="displaytag" keepStatus="true"
					   name="${us.following}" requestURI="${requestURI}" id="row">

			<spring:message code="user.name" var="name" />
			<display:column property="name" title="${name}" sortable="true" />
			<spring:message code="user.surname" var="surname" />
			<display:column property="surname" title="${surname}" sortable="true" />
			<spring:message code="user.picture" var="picture" />
			<display:column title="${picture}">
				<img src="${row.picture}" width="80" height="80" >
			</display:column>
			<spring:message code="user.address" var="address" />
			<display:column property="address" title="${address}"
							sortable="true" />
			<spring:message code="user.email" var="email" />
			<display:column property="email" title="${email}" sortable="true" />
			<spring:message code="user.phone" var="phone" />
			<display:column property="phone" title="${phone}" sortable="true" />


		</display:table>

		<spring:message code="user.walks" var="suc"/>
		<h3><jstl:out value="${suc}"/></h3>
		<display:table pagesize="5" class="displaytag" keepStatus="true"
					   name="walks" requestURI="${requestURI}" id="row">

			<spring:message code="walk.name" var="name" />
			<display:column property="name" title="${name}" sortable="true" />
			<spring:message code="walk.compostelaStatus" var="compostelaStatus" />
			<display:column property="compostelaStatus" title="${compostelaStatus}" sortable="true" />
			<spring:message code="walk.denyReason" var="denyReason" />
			<display:column property="denyReason" title="${denyReason}" sortable="true" />


		</display:table>
	</div>
	<div class="col-4">

		<img src="${us.picture}">
		<hr>
		<jstl:out value="${us.name}"/>
		<jstl:out value="${us.surname}"/>
		<br>
		<jstl:out value="${us.email}"/>
		<br>
		<jstl:out value="${us.phone}"/>
		<br>
		<jstl:out value="${us.address}"/>

	</div>
</div>
