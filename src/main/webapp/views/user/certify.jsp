<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%--
  ~ Copyright  2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
  --%>
<!-- Listing grid -->

<display:table pagesize="15" class="displaytag" keepStatus="true"
	name="registrations" requestURI="${requestURI}" id="row">
	<c:set var="localeCode" value="${pageContext.response.locale}" />
	
	<spring:message code="user.name" var="name" />
	<display:column property="hosted.name" title="${name}" sortable="true" />
	<spring:message code="user.surname" var="surname" />
	<display:column property="hosted.surname" title="${surname}" sortable="true" />
	<spring:message code="user.city" var="hostal" />
	<display:column property="hostal.city" title="${hostal}" sortable="true" />
	<spring:message code="registration.startDate" var="registrations" />
	<c:choose>
		<c:when test="${localeCode == 'en'}">
			<display:column property="startDate" title="${startDate}" sortable="true"
				format="{0,date,yyyy/MM/dd}" />
		</c:when>
		<c:when test="${localeCode == 'es'}">
			<display:column property="startDate" title="${startDate}" sortable="true"
				format="{0,date,dd-MM-yyyy}" />
		</c:when>
	</c:choose>

</display:table>	