<%--
 * header.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<div>
	<img src="images/logo.png" alt="ACME, Inc.  Your certification Company" />
</div>


<nav class="navbar navbar-expand-lg navbar-light bg-light">
	<a class="navbar-brand" href="#"><spring:message code="master.page.home" /></a>
	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
	</button>

	<div class="collapse navbar-collapse" id="navbarSupportedContent">
		<ul class="navbar-nav mr-auto">


			<security:authorize access="isAnonymous()">
				<%--Esto es un item de men� normal--%>

				<li class="nav-item" ><a class="nav-link" href="security/login.do"><spring:message code="master.page.login" /></a></li>
			<li class="nav-item" ><a class="nav-link" href="user/create.do"><spring:message code="master.page.user.register" /></a></li>
			<li class="nav-item" ><a class="nav-link" href="agent/create.do"><spring:message code="master.page.agent.register" /></a></li>
			<li class="nav-item" ><a class="nav-link" href="route/list.do"><spring:message code="master.page.route.list" /></a></li>
			<li class="nav-item" ><a class="nav-link" href="user/list.do"><spring:message code="master.page.user.lists"/></a></li>
			<li class="nav-item" ><a class="nav-link" href="hostal/validlist.do"><spring:message code="master.page.hostal.lists"/></a></li>

			</security:authorize>

				<security:authorize access="isAuthenticated()">

					<li class="nav-item" ><a class="nav-link" href="j_spring_security_logout"><spring:message code="master.page.profile.logout"/></a></li>
					<li class="nav-item" ><a class="nav-link" href="user/list.do"><spring:message code="master.page.user.lists"/></a></li>
					<li class="nav-item" ><a class="nav-link" href="route/list.do"><spring:message code="master.page.route.list" /></a></li>
					<li class="nav-item" ><a class="nav-link" href="hostal/validlist.do"><spring:message code="master.page.hostal.lists"/></a></li>
				</security:authorize>




			<security:authorize access="hasRole('ADMINISTRATOR')">
				<li class="nav-item" ><a class="nav-link" href="administrator/dashboard.do"><spring:message code="master.page.administrator.dashboard"/></a></li>
                <li class="nav-item" ><a class="nav-link" href="route/listInternal.do"><spring:message code="master.page.administrator.routes"/></a></li>
				<li class="nav-item" ><a class="nav-link" href="administrator/utilsView.do"><spring:message code="master.page.administrator.utils"/></a></li>
				<li class="nav-item" ><a class="nav-link" href="administrator/listTabooChirps.do"><spring:message code="master.page.administrator.chirps"/></a></li>
				<li class="nav-item" ><a class="nav-link" href="administrator/listTabooComments.do"><spring:message code="master.page.administrator.comments"/></a></li>
				<li class="nav-item" ><a class="nav-link" href="administrator/listTabooAdvertisement.do"><spring:message code="master.page.administrator.advertisement"/></a></li>
				<li class="nav-item" ><a class="nav-link" href="administrator/walksAwaiting.do"><spring:message code="master.page.administrator.walks"/></a></li>

			</security:authorize>

			<security:authorize access="hasRole('USER')">
				<li class="nav-item" ><a class="nav-link" href="route/myList.do"><spring:message code="master.page.route.myList"/></a></li>
				<li class="nav-item" ><a class="nav-link" href="user/followers.do"><spring:message code="master.page.user.followers"/></a></li>
				<li class="nav-item" ><a class="nav-link" href="user/following.do"><spring:message code="master.page.user.following"/></a></li>
				<li class="nav-item" ><a class="nav-link" href="chirp/list.do"><spring:message code="master.page.user.chirp"/></a></li>
				<li class="nav-item" ><a class="nav-link" href="chirp/followingChirp.do"><spring:message code="master.page.user.followingChirp"/></a></li>
				<li class="nav-item" ><a class="nav-link" href="walk/list.do"><spring:message code="master.page.user.walk"/></a></li>
			</security:authorize>

			<security:authorize access="hasRole('INNKEEPER')">
				<li class="nav-item" ><a class="nav-link" href="user/list.do"><spring:message code="master.page.user.list"/></a></li>
				<li class="nav-item" ><a class="nav-link" href="user/certify.do"><spring:message code="master.page.user.certify"/></a></li>
				<li class="nav-item" ><a class="nav-link" href="hostal/hikesAwarding.do"><spring:message code="master.page.in.pendinghikes"/></a></li>
				<li class="nav-item dropdown">
					<a class="nav-link dropdown-toggle" href="#" id="advertisementDropdown2" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						<spring:message code="master.page.hostal" />
					</a>
					<div class="dropdown-menu" aria-labelledby="advertisementDropdown2">
						<a class="dropdown-item" href="hostal/list.do"><spring:message code="master.page.hostal.list" /></a>
					</div>
				</li>
			</security:authorize>

			<security:authorize access="hasRole('AGENT')">

				<%--Esto es un item de desplegable--%>
				<li class="nav-item dropdown">
					<a class="nav-link dropdown-toggle" href="#" id="advertisementDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						<%--Aqu� va el titulo del desplegable--%>
						<spring:message code="master.page.advertisement" />
					</a>
					<div class="dropdown-menu" aria-labelledby="navbarDropdown">
						<%--Y aqu� los elementos con el siguiente formato--%>
						<a class="dropdown-item" href="advertisement/create.do"><spring:message code="master.page.advertisement.create" /></a>
						<a class="dropdown-item" href="advertisement/listMy.do"><spring:message code="master.page.advertisement.listMy" /></a>

					</div>
				</li>


				<li class="nav-item" ><a class="nav-link" href="hike/listOFAgentOnAds.do"><spring:message code="master.page.hikes.onAds" /></a></li>
				<li class="nav-item" ><a class="nav-link" href="hike/listOFAgentWithNoAds.do"><spring:message code="master.page.hikes.NoAds" /></a></li>
                <li class="nav-item" ><a class="nav-link" href="agent/editCreditCard.do"><spring:message code="master.page.agent.editCredit" /></a></li>
			</security:authorize>



		</ul>
	</div>
</nav>




<div>
	<a href="?language=en">en</a> | <a href="?language=es">es</a>
</div>

