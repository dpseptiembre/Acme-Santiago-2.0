<%--
 * list.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<display:table pagesize="5" class="displaytag" keepStatus="true"
	name="walks" requestURI="${requestURI}" id="row">


		<display:column>
		<a href="walk/view.do?walkId=${row.id}"> <spring:message
				code="general.details" />
		</a>
		</display:column>


	<security:authorize access="hasRole('ADMINISTRATOR')">
		<display:column>
			<a class="badge badge-success" href="administrator/approveCompostela.do?walkId=${row.id}" onclick="return confirm('<spring:message code="general.confirm"/>') "><spring:message code="walk.approve"/></a>
		</display:column>
		<display:column>
			<a class="badge badge-danger" href="administrator/denyCompostela.do?walkId=${row.id}" onclick="return confirm('<spring:message code="general.confirm"/>') "><spring:message code="walk.deny"/></a>
		</display:column>
		<display:column>
			<a class="badge badge-warning" href="administrator/suggest.do?walkId=${row.id}" onclick="return confirm('<spring:message code="general.confirm"/>') "><spring:message code="walk.suggest"/></a>
		</display:column>
		<spring:message code="walk.sugestedStatus" var="sugestedStatus" />
		<display:column property="sugestedStatus" title="${sugestedStatus}" sortable="true" />
	</security:authorize>

	<spring:message code="walk.name" var="name" />
	<display:column property="name" title="${name}" sortable="true" />
	<spring:message code="walk.compostelaStatus" var="compostelaStatus" />
	<display:column property="compostelaStatus" title="${compostelaStatus}" sortable="true" />
	<spring:message code="walk.denyReason" var="denyReason" />
	<display:column property="denyReason" title="${denyReason}" sortable="true" />

	
	<security:authorize access="hasRole('USER')">
		<display:column>
			<jstl:if test="${row.compostelaStatus != 'ACCEPTED'}">
				<jstl:if test="${row.compostelaStatus != 'PENDING'}">
					<a href="walk/request.do?walkId=${row.id}"> <spring:message
							code="walk.request" />
					</a>
	
				</jstl:if>
			</jstl:if>	
		</display:column>

    <display:column>
        <jstl:if test="${row.compostelaStatus == 'ACCEPTED'}">
                    <a href="administrator/generate.do?walkId=${row.id}"> <spring:message
                            code="walk.generate.compostela" />
                    </a>
        </jstl:if>
    </display:column>
	</security:authorize>
</display:table>	