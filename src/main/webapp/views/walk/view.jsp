<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>


<%--
  ~ Copyright  2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
  --%>
<!-- Listing grid -->
<c:set var="localeCode" value="${pageContext.response.locale}" />
<div class="container">


	<h2><jstl:out value="${wk.name}"/></h2>
	<p><jstl:out value="${wk.author}"/></p>


	<security:authorize access="hasRole('USER')">
		<jstl:if test="${my}">

			<jstl:if test="${row.compostelaStatus != 'ACCEPTED'}">
				<jstl:if test="${row.compostelaStatus != 'PENDING'}">
					<a href="walk/request.do?walkId=${wk.id}"> <spring:message
							code="walk.request" />
					</a>

				</jstl:if>
			</jstl:if>



			<jstl:if test="${row.compostelaStatus == 'ACCEPTED'}">
				<a href="administrator/generate.do?walkId=${wk.id}"> <spring:message
						code="walk.generate.compostela" />
				</a>
			</jstl:if>
		</jstl:if>

	</security:authorize>


	<spring:message code="route.hikes.incomplete" var="suc"/>
	<h3><jstl:out value="${suc}"/></h3>
	<display:table pagesize="15" class="displaytag" keepStatus="true"
				   name="${in}" requestURI="${requestURI}" id="row">

		<security:authorize access="isAuthenticated()">
			<display:column>
				<a href="hike/view.do?hikeId=${row.id}"> <spring:message code="general.details" />
				</a>
			</display:column>
		</security:authorize>

		<spring:message code="hike.name" var="name" />
		<display:column property="name" title="${name}" sortable="true" />
		<spring:message code="hike.length" var="length" />
		<display:column property="lgt" title="${length}" sortable="true" />
		<spring:message code="hike.origin" var="origin" />
		<display:column property="origin" title="${origin}" sortable="true" />
		<spring:message code="hike.destination" var="destination" />
		<display:column property="destination" title="${destination}" sortable="true" />
		<spring:message code="hike.level" var="level" />
		<display:column property="level" title="${level}" sortable="true" />
		<spring:message code="hike.day" var="day" />
		<c:choose>
		<c:when test="${localeCode == 'en'}">
			<display:column property="day" title="${day}" sortable="true"
				format="{0,date,yyyy/MM/dd}" />
		</c:when>
		<c:when test="${localeCode == 'es'}">
			<display:column property="day" title="${day}" sortable="true"
				format="{0,date,dd-MM-yyyy}" />
		</c:when>
	</c:choose>

		<jstl:if test="${my}">
			<jstl:if test="${row.completedHike == false}">
				<display:column>
					<a href="walk/registration.do?hikeId=${row.id}"> <spring:message code="general.accomdation" />
					</a>
				</display:column>
			</jstl:if>

		</jstl:if>

	</display:table>



	<spring:message code="route.hikes.complete" var="suc"/>
	<h3><jstl:out value="${suc}"/></h3>
	<display:table pagesize="15" class="displaytag" keepStatus="true"
				   name="${com}" requestURI="${requestURI}" id="row">

		<security:authorize access="isAuthenticated()">
			<display:column>
				<a href="hike/view.do?hikeId=${row.id}"> <spring:message code="general.details" />
				</a>
			</display:column>
		</security:authorize>

		<spring:message code="hike.name" var="name" />
		<display:column property="name" title="${name}" sortable="true" />
		<spring:message code="hike.length" var="length" />
		<display:column property="lgt" title="${length}" sortable="true" />
		<spring:message code="hike.origin" var="origin" />
		<display:column property="origin" title="${origin}" sortable="true" />
		<spring:message code="hike.destination" var="destination" />
		<display:column property="destination" title="${destination}" sortable="true" />
		<spring:message code="hike.level" var="level" />
		<display:column property="level" title="${level}" sortable="true" />
		<spring:message code="hike.day" var="day" />
		<c:choose>
		<c:when test="${localeCode == 'en'}">
			<display:column property="day" title="${day}" sortable="true"
				format="{0,date,yyyy/MM/dd}" />
		</c:when>
		<c:when test="${localeCode == 'es'}">
			<display:column property="day" title="${day}" sortable="true"
				format="{0,date,dd-MM-yyyy}" />
		</c:when>
	</c:choose>
		<spring:message code="hike.accomodationStatus" var="accomodationStatus" />
		<display:column property="accomodationStatus" title="${accomodationStatus}" sortable="true" />
		<spring:message code="hike.accommodation" var="accommodation" />
		<display:column property="accommodation" title="${accommodation}" sortable="true" />





	</display:table>




	<spring:message code="route.comments" var="suc"/>
	<h3><jstl:out value="${suc}"/></h3>
	<display:table pagesize="5" class="displaytag" keepStatus="true"
				   name="${wk.comments}" requestURI="${requestURI}" id="row">

		<security:authorize access="hasRole('ADMINISTRATOR')">
			<display:column>
				<a class="badge badge-danger" href="administrator/removeComment.do?commentId=${row.id}" onclick="return confirm('<spring:message code="general.confirm"/>') "><spring:message code="general.delete"/></a>
			</display:column>
		</security:authorize>

		<spring:message code="chirp.title" var="title" />
		<display:column property="title" title="${title}" sortable="true" />
		<spring:message code="comment.stars" var="stars" />
		<display:column property="stars" title="${stars}" sortable="true" />
		<spring:message code="comment.body" var="body" />
		<display:column property="body" title="${body}" sortable="true" />
		<c:forEach var = "listValue" items = "${row.pictures}">
			<spring:message code="amenities.pictures" var="pictures" />
			<display:column title="${pictures}">
				<img src="${listValue}" width="130" height="130" >
			</display:column>
		</c:forEach>
	</display:table>



</div>