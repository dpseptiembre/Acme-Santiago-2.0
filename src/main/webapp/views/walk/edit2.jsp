<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>


<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@ taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<form:form action="walk/edit2.do" modelAttribute="walk">

	<form:hidden path="id" />
	<form:hidden path="version" />
	<form:hidden path="comments" />
	<form:hidden path="denyReason" />
	<form:hidden path="compostelaStatus" />
	<form:hidden path="hikes" />
	
	<acme:textbox path="name" code="walk.name" />
	<br />

	
		<!---------------------------- BOTONES -------------------------->


	<input type="submit" name="save"
		value="<spring:message code="walk.save" />" />


    <input type="button" name="cancel"
		value="<spring:message code="walk.cancel" />"
		onclick="javascript: window.location.replace('walk/list.do')" />

</form:form>