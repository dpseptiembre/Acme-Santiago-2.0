<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>


<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@ taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<form:form action="walk/edit.do" modelAttribute="walk">

	<form:hidden path="id" />
	<form:hidden path="version" />
	<form:hidden path="hostal" />
	
	<acme:textbox path="name" code="walk.name" />
	<br />
	<acme:textbox path="description" code="walk.description" />
	<br />
	<acme:textbox path="pictures" code="walk.pictures" />
	<br />
	
		<!---------------------------- BOTONES -------------------------->


	<input type="submit" name="save"
		value="<spring:message code="amenities.save" />" />

	<input type="submit" name="delete"
		value="<spring:message code="amenities.delete"/>"
		onclick="return confirm('<spring:message code="amenities.confirm.delete" />')" />&nbsp;

    <input type="button" name="cancel"
		value="<spring:message code="amenities.cancel" />"
		onclick="window.location.replace('hostal/list.do')" />

</form:form>