<%@page language="java" contentType="text/html; charset=ISO-8859-1"
        pageEncoding="ISO-8859-1" %>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="security"
          uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="display" uri="http://displaytag.sf.net" %>
<%@ taglib prefix="acme" tagdir="/WEB-INF/tags" %>

<%--
  ~ Copyright  2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
  --%>
<div class="container">
<form:form action="walk/registrationsub.do" modelAttribute="hike">

    <form:hidden path="id"/>
    <form:hidden path="version"/>
    <form:hidden path="hidden"/>
    <form:hidden path="ads"/>
    <form:hidden path="route"/>
    <form:hidden path="accomodationStatus"/>
    <form:hidden path="completedHike"/>
    <form:hidden path="pictures"/>
    <form:hidden path="lgt"/>
    <form:hidden path="origin"/>
    <form:hidden path="day"/>
    <form:hidden path="destination"/>
    <form:hidden path="level"/>
    <form:hidden path="comments"/>
    <form:hidden path="name"/>


    <acme:textbox path="accomodationDate" code="hike.accomodationDate"/>


    <form:label path="accommodation">
        <spring:message code="hike.accommodation"/>:
    </form:label>
    <form:select path="accommodation" code="hike.accommodations">
        <form:option value="0" label="----" />
        <form:options items="${hostals}" itemValue="id" itemLabel="name" />
    </form:select>



    <br>

    <!---------------------------- BOTONES -------------------------->

    <acme:submit name="save" code="general.save"/>
    <input type="button" class="btn btn-danger" name="cancel"
           value="<spring:message code="general.cancel" />"
           onclick="relativeRedir('welcome/index.do');"/>
    <br/>

</form:form>

</div>
