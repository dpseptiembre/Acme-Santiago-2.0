<%--
 * list.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<display:table pagesize="5" class="displaytag" keepStatus="true"
	name="amenitieses" requestURI="${requestURI}" id="row">
	
	<security:authorize access="hasRole('INNKEEPER')">
		<display:column>
			<a href="amenities/edit.do?amenitiesId=${row.id}"> <spring:message
					code="amenities.edit" />
			</a>
		</display:column>
	</security:authorize>
	
	<spring:message code="amenities.name" var="name" />
	<display:column property="name" title="${name}" sortable="true" />
	
	<spring:message code="amenities.description" var="description" />
	<display:column property="description" title="${description}" sortable="true" />
	
	<%--<spring:message code="amenities.pictures" var="pictures" />--%>
	<%--<display:column property="pictures" title="${pictures}" sortable="true" />--%>




    <c:forEach var = "listValue" items = "${row.pictures}">
        <spring:message code="amenities.pictures" var="pictures" />
        <display:column title="${pictures}">
            <img src="${listValue}" width="70" height="70" >
        </display:column>
    </c:forEach>

</display:table>	