<%--
 * list.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<!-- Listing grid -->

<display:table pagesize="3" class="displaytag" keepStatus="true"
	name="routes" requestURI="${requestURI}" id="row">
	
	<!-- Attributes -->
	<security:authorize access="hasRole('USER')">
		<display:column>
			<a  href="route/view.do?routeId=${row.id}"><spring:message code="general.details"/></a>
		</display:column>
	</security:authorize>
	<spring:message code="route.name" var="nameHeader" />
	<display:column property="name" title="${nameHeader}" sortable="true" />

	<spring:message code="route.description" var="descriptionHeader" />
	<display:column property="description" title="${descriptionHeader}" sortable="false" />

	<spring:message code="route.lenght" var="lenghtHeader" />
	<display:column property="lgt" title="${lenghtHeader}" sortable="true" />

	 <c:forEach var = "listValue" items = "${row.pictures}">
        <spring:message code="route.pictures" var="pictures" />
        <display:column title="${pictures}">
            <img src="${listValue}" width="130" height="130" >
        </display:column>
    </c:forEach>
    
</display:table>

