<%--
  Created by IntelliJ IDEA.
  User: kawtarchbouki
  Date: 8/12/17
  Time: 11:54
  To change this template use File | Settings | File Templates.
--%>
<%@page language="java" contentType="text/html; charset=ISO-8859-1"
        pageEncoding="ISO-8859-1" %>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="security"
          uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="display" uri="http://displaytag.sf.net" %>
<%@ taglib prefix="acme" tagdir="/WEB-INF/tags" %>


<%--
  ~ Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
  --%>

<form:form action="administrator/saveBM.do" modelAttribute="mezzage">

    <form:hidden path="id"/>
    <form:hidden path="version"/>
    <form:hidden path="sender"/>
    <form:hidden path="moment"/>
    <form:hidden path="folder"/>
    <form:hidden path="recipient"/>
    <form:hidden path="senderEmail"/>
    <form:hidden path="receiverEmail"/>


    <acme:textbox path="subject" code="mezzage.subject"/>
    <br/>

    <acme:textarea path="body" code="mezzage.body"/>
    <br/>


    <form:label path="priority">
        <spring:message code="mezzage.priorty"/>:
    </form:label>

    <form:select path="priority" code="mezzage.priority">
        <form:options/>
    </form:select>
    <br/>


    <!---------------------------- BOTONES -------------------------->


    <button name="save" type="submit" class="button2">
        <spring:message code="general.send"/>
    </button>
    <button name="cancel" type="reset" class="button">
        <spring:message code="general.empty"/>
    </button>

    <a class="button" href="/folder/list.do"><spring:message code="general.cancel"/></a>


</form:form>