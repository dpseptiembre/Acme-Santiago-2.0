<%--
 * list.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<display:table pagesize="5" class="displaytag" keepStatus="true"
	name="hostals" requestURI="${requestURI}" id="row">
	
	<spring:message code="hostal.name" var="name" />
	<display:column property="name" title="${name}" sortable="true" />

    <spring:message code="hostal.badge" var="badge" />
    <display:column title="${badge}">
        <img src="${row.badge}" width="80" height="80" >
    </display:column>


    <spring:message code="hostal.street" var="street" />
	<display:column property="street" title="${street}" sortable="true" />
	
	<spring:message code="hostal.number" var="number" />
	<display:column property="number" title="${number}" sortable="true" />
	
	<spring:message code="hostal.city" var="city" />
	<display:column property="city" title="${city}" sortable="true" />
	
	<spring:message code="hostal.phoneNumber" var="phoneNumber" />
	<display:column property="phoneNumber" title="${phoneNumber}" sortable="true" />
	
	<spring:message code="hostal.email" var="email" />
	<display:column property="email" title="${email}" sortable="true" />
	
	<spring:message code="hostal.web" var="web" />
	<display:column property="web" title="${web}" sortable="true" />
	
	
	<display:column>
				<a href="amenities/list.do?hostalId=${row.id}"> <spring:message
					code="amenities.list" />
				</a>
	</display:column>
	<security:authorize access="hasRole('INNKEEPER')">
	<display:column>
				<a href="amenities/create.do?hostalId=${row.id}"> <spring:message
					code="amenities.list.create" />
				</a>
	</display:column>
	</security:authorize>

	<display:column>
		<a href="hostal/view.do?hostalId=${row.id}"> <spring:message
				code="general.details" />
		</a>
	</display:column>


</display:table>	