<%--
 * list.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="row">
	<div class="col-8">
		<h2><jstl:out value="${in.name}"/></h2>
		<img src="${in.badge}">

		<hr>
		<display:table pagesize="5" class="displaytag" keepStatus="true"
					   name="${in.amenities}" requestURI="${requestURI}" id="row">



			<spring:message code="amenities.name" var="name" />
			<display:column property="name" title="${name}" sortable="true" />

			<spring:message code="amenities.description" var="description" />
			<display:column property="description" title="${description}" sortable="true" />


		</display:table>

	</div>
	<div class="col-4">
		<jstl:out value="${in.street}"/>
		<jstl:out value="${in.number}"/>
		<jstl:out value="${in.city}"/>
		<hr>
		<jstl:out value="${in.phoneNumber}"/>
		<br>
		<jstl:out value="${in.email}"/>
		<br>
		<a href="${in.web}">${in.web}</a>
	</div>
</div>