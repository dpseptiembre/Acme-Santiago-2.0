<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>


<%--
  ~ Copyright  2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
  --%>
<!-- Listing grid -->
<display:table pagesize="15" class="displaytag" keepStatus="true"
	name="hikes" requestURI="${requestURI}" id="row">
<c:set var="localeCode" value="${pageContext.response.locale}" />
	<security:authorize access="isAuthenticated()">
		<display:column>
			<a href="hike/view.do?hikeId=${row.id}"> <spring:message code="general.details" />
			</a>
		</display:column>
	</security:authorize>

	<security:authorize access="hasRole('INNKEEPER')">
		<display:column>
			<a href="hostal/approve.do?hikeId=${row.id}"> <spring:message code="general.approve" />
			</a>
		</display:column>

		<display:column>
			<a href="hostal/deny.do?hikeId=${row.id}"> <spring:message code="general.deny" />
			</a>
		</display:column>
	</security:authorize>

	<spring:message code="hike.name" var="name1" />
	<display:column property="name" title="${name1}" sortable="true" />

	<spring:message code="hike.length" var="length1" />
	<display:column property="lgt" title="${length1}" sortable="true" />

	<spring:message code="hike.origin" var="origin1" />
	<display:column property="origin" title="${origin1}" sortable="true" />

	<spring:message code="hike.destination" var="destination1" />
	<display:column property="name" title="${destination1}" sortable="true" />

	<spring:message code="hike.level" var="level1" />
	<display:column property="level" title="${level1}" sortable="true" />

	<spring:message code="hike.day" var="day1" />
	<c:choose>
		<c:when test="${localeCode == 'en'}">
			<display:column property="day" title="${day1}" sortable="true"
				format="{0,date,yyyy/MM/dd}" />
		</c:when>
		<c:when test="${localeCode == 'es'}">
			<display:column property="day" title="${day1}" sortable="true"
				format="{0,date,dd-MM-yyyy}" />
		</c:when>
	</c:choose>




</display:table>
