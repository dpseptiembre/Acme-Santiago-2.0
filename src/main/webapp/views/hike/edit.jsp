<%@page language="java" contentType="text/html; charset=ISO-8859-1"
        pageEncoding="ISO-8859-1" %>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="security"
          uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="display" uri="http://displaytag.sf.net" %>
<%@ taglib prefix="acme" tagdir="/WEB-INF/tags" %>

<%--
  ~ Copyright  2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
  --%>
<div class="container">
<form:form action="hike/edit.do" modelAttribute="hike">

    <form:hidden path="id"/>
    <form:hidden path="version"/>
    <form:hidden path="hidden"/>
    <form:hidden path="ads"/>
    <form:hidden path="comments"/>
    <form:hidden path="route"/>
    <form:hidden path="accomodationStatus"/>
    <form:hidden path="completedHike"/>
    <form:hidden path="accomodationDate"/>

    <acme:textbox path="name" code="hike.name"/>
    <acme:textbox path="lgt" code="hike.length"/>
    <acme:textbox path="origin" code="hike.origin"/>
    <acme:textbox path="day" code="hike.day"/>
    <acme:textbox path="destination" code="hike.destination"/>


    <form:label path="level">
        <spring:message code="hike.level"/>:
    </form:label>
    <form:select path="level" code="hike.level">
        <form:options/>
    </form:select>
    <input placeholder="Picture to add" name="pictures" class="form-group" type="text"/>


    <br>

    <!---------------------------- BOTONES -------------------------->

    <acme:submit name="save" code="general.save"/>
    <input type="button" class="btn btn-danger" name="cancel"
           value="<spring:message code="general.cancel" />"
           onclick="relativeRedir('welcome/index.do');"/>
    <br/>

</form:form>

</div>
