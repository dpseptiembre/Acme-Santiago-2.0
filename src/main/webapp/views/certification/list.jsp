<%--
 * list.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!-- Listing grid -->

<display:table pagesize="5" class="displaytag" keepStatus="true"
	name="certifications" requestURI="${requestURI}" id="row">
	<c:set var="localeCode" value="${pageContext.response.locale}" />
	<!-- Action links -->
	
	<!-- Attributes -->

	<spring:message code="certification.title" var="titleHeader" />
	<display:column property="title" title="${titleHeader}" sortable="true" />

	<spring:message code="certification.description" var="descriptionHeader" />
	<display:column property="description" title="${descriptionHeader}" sortable="false" />

	<spring:message code="certification.extinctionDate" var="extinctionHeader" />
	<c:choose>
		<c:when test="${localeCode == 'en'}">
			<display:column property="extinctionDate" title="${extinctionHeader}" sortable="true"
				format="{0,date,yyyy/MM/dd}" />
		</c:when>
		<c:when test="${localeCode == 'es'}">
			<display:column property="extinctionDate" title="${extinctionHeader}" sortable="true"
				format="{0,date,dd-MM-yyyy}" />
		</c:when>
	</c:choose>

	<spring:message code="certification.fee" var="feeHeader" />
	<display:column property="fee" title="${feeHeader}"	sortable="true" />
	
</display:table>

