<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>


<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@ taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<form:form action="registration/edit.do" modelAttribute="registration">
	
	<form:hidden path="id" />
	<form:hidden path="version" />
	<form:hidden path="startDate" />
	<form:hidden path="hosted" />
	
	<acme:select2 path="hostal" code="registration.hostal"
		items="${hostals}" itemLabel="name" />
	<br>
	
	<input type="submit" name="save"
		value="<spring:message code="registration.save" />" />&nbsp; 
	<jstl:if test="${registration.id != 0}">
	<input type="submit" name="delete"
			value="<spring:message code="registration.delete" />"
			onclick="return confirm('<spring:message code="registration.confirm.delete" />')" />&nbsp;
	</jstl:if>
	<input type="button" name="cancel"
		value="<spring:message code="registration.cancel" />"
		onclick="relativeRedir('registration/list.do');" />
	<br />

</form:form>