<%--
 * list.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<display:table pagesize="5" class="displaytag" keepStatus="true"
	name="registrations" requestURI="${requestURI}" id="row">
	<c:set var="localeCode" value="${pageContext.response.locale}" />
	<spring:message code="registration.hostal" var="hostal" />
	<display:column property="hostal" title="${hostal}" sortable="true" />

	<spring:message code="registration.startDate" var="startDate" />
		<c:choose>
		<c:when test="${localeCode == 'en'}">
			<display:column property="startDate" title="${startDate}" sortable="true"
				format="{0,date,yyyy/MM/dd}" />
		</c:when>
		<c:when test="${localeCode == 'es'}">
			<display:column property="startDate" title="${startDate}" sortable="true"
				format="{0,date,dd-MM-yyyy}" />
		</c:when>
	</c:choose>
	
		<spring:message code="registration.endDate" var="endDate" />
			<c:choose>
		<c:when test="${localeCode == 'en'}">
			<display:column property="endDate" title="${endDate}" sortable="true"
				format="{0,date,yyyy/MM/dd}" />
		</c:when>
		<c:when test="${localeCode == 'es'}">
			<display:column property="endDate" title="${endDate}" sortable="true"
				format="{0,date,dd-MM-yyyy}" />
		</c:when>
	</c:choose>

	<spring:message code="registration.hosted" var="hosted" />
	<display:column property="hosted.name" title="${hosted}" sortable="false" />
	
</display:table>	