<%@page language="java" contentType="text/html; charset=ISO-8859-1"
        pageEncoding="ISO-8859-1" %>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="security"
          uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="display" uri="http://displaytag.sf.net" %>
<%@ taglib prefix="acme" tagdir="/WEB-INF/tags" %>

<%--
  ~ Copyright  2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
  --%>
<div class="container">
<form:form action="agent/saveCreditCard.do" modelAttribute="creditCard">

    <acme:textbox path="holderName" code="agent.creditCard.holderName"/>
    <acme:textbox path="number" code="agent.creditCard.number"/>
    <acme:textbox path="year" code="agent.creditCard.expirationYear"/>
    <acme:textbox path="month" code="agent.creditCard.expirationMonth"/>
    <acme:textbox path="CVV" code="agent.creditCard.CVV"/>
    <form:label path="type">
        <spring:message code="agent.creditCard.brandName"/>:
    </form:label>
    <form:select path="type" code="agent.creditCard.brandName">
        <form:options/>
    </form:select>


    <!---------------------------- BOTONES -------------------------->

    <acme:submit name="save" code="general.save"/>
    <input type="button" class="btn btn-danger" name="cancel"
           value="<spring:message code="general.cancel" />"
           onclick="relativeRedir('welcome/index.do');"/>
    <br/>

</form:form>

</div>
