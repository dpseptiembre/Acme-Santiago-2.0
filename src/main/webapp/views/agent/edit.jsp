<%@page language="java" contentType="text/html; charset=ISO-8859-1"
        pageEncoding="ISO-8859-1" %>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="security"
          uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="display" uri="http://displaytag.sf.net" %>
<%@ taglib prefix="acme" tagdir="/WEB-INF/tags" %>

<%--
  ~ Copyright  2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
  --%>
<div class="container">
<form:form action="agent/edit.do" modelAttribute="agent">

    <form:hidden path="id"/>
    <form:hidden path="version"/>
    <form:hidden path="userAccount"/>

	<acme:textarea path="picture" code="agent.picture"/>
	<acme:textarea path="email" code="agent.email"/>
	<acme:textarea path="phone" code="agent.phone"/>
    <acme:textarea path="surname" code="agent.surname"/>
	<acme:textarea path="address" code="agent.address"/>
    <acme:textarea path="name" code="agent.name"/>


    <br>

    <h1><spring:message code="credit.card"/>:</h1>
    <br/>
    <acme:textarea path="creditCard.holderName" code="agent.creditCard.holderName"/>
    <acme:textarea path="creditCard.number" code="agent.creditCard.number"/>
    <acme:textarea path="creditCard.year" code="agent.creditCard.expirationYear"/>
    <acme:textarea path="creditCard.month" code="agent.creditCard.expirationMonth"/>
    <acme:textarea path="creditCard.CVV" code="agent.creditCard.CVV"/>
    <form:label path="creditCard.type">
        <spring:message code="agent.creditCard.brandName"/>:
    </form:label>
    <form:select path="creditCard.type" code="agent.creditCard.brandName">
        <form:options/>
    </form:select>

    <hr>

    <!---------------------------- BOTONES -------------------------->

    <acme:submit name="save" code="general.save"/>
    <input type="button" class="btn btn-danger" name="cancel"
           value="<spring:message code="general.cancel" />"
           onclick="relativeRedir('welcome/index.do');"/>
    <br/>

</form:form>

</div>
