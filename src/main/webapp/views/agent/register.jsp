<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@ taglib prefix="acme" tagdir="/WEB-INF/tags"%>


<div class="container">
	<form:form action="agent/register.do" modelAttribute="agent">

		<form:hidden path="id" />
		<form:hidden path="version" />
		<form:hidden path="userAccount.authorities" />


		<acme:textarea path="name" code="agent.name" />
		<acme:textarea path="surname" code="agent.surname" />
		<acme:textarea path="picture" code="agent.picture" />
		<acme:textarea path="email" code="agent.email" />
		<acme:textarea path="phone" code="agent.phone" />
		<acme:textarea path="address" code="agent.address" />





		<br />
		<h1>User Account</h1>
		<br>
		<form:label path="userAccount.username">
			<spring:message code="agent.username" />:
    </form:label>
		<form:input path="userAccount.username" />
		<form:errors cssClass="error" path="userAccount.username" />
		<br />
		<br>
		<form:label path="userAccount.password">
			<spring:message code="agent.password" />:
    </form:label>
		<form:password path="userAccount.password" />
		<form:errors cssClass="error" path="userAccount.password" />
		<br />
		<br />

		<!---------------------------- BOTONES -------------------------->
		<input class="btn btn-info" type="submit" name="save"
			value="<spring:message code="agent.save" />" />
		<a class="btn btn-danger" href="welcome/index.do"><spring:message
				code="general.cancel" /></a>
	</form:form>
</div>
