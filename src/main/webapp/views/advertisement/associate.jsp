<%@page language="java" contentType="text/html; charset=ISO-8859-1"
        pageEncoding="ISO-8859-1" %>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="security"
          uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="display" uri="http://displaytag.sf.net" %>
<%@ taglib prefix="acme" tagdir="/WEB-INF/tags" %>
<%--<%@ taglib uri="http://bootstrapjsp.org/" prefix="b" %>--%>
<div class="container">
    <form:form action="advertisement/associate.do" modelAttribute="advertisement">


        <form:hidden path="id"/>
        <form:hidden path="version"/>
        <form:hidden path="creator"/>
        <form:hidden path="hidden"/>
        <form:hidden path="title"/>
        <form:hidden path="banner"/>
        <form:hidden path="displayDays"/>
        <form:hidden path="routes"/>

        <acme:select path="hike" code="advertisement.hike" items="${hikes}" itemLabel="name"/>


        <br>
        <br>

        <!---------------------------- BOTONES -------------------------->
        <input class="button2" type="submit" name="save"
               value="<spring:message code="general.save"/>"
        />&nbsp;

        <input class="button" type="button" name="cancel"
               value="<spring:message code="general.cancel" />"
               onclick="relativeRedir('advertisement/listMy.do');"/>
        <br/>


    </form:form>
</div>