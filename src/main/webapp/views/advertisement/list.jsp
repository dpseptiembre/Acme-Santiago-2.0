<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>


<%--
  ~ Copyright  2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
  --%>
<!-- Listing grid -->
<display:table pagesize="15" class="displaytag" keepStatus="true"
	name="advertisements" requestURI="${requestURI}" id="row">

    <security:authorize access="hasRole('ADMINISTRATOR')">
        <display:column>
            <a class="badge badge-danger" href="administrator/removeAdvertisements.do?advertisementId=${row.id}" onclick="return confirm('<spring:message code="general.confirm"/>') "><spring:message code="general.delete"/></a>
        </display:column>
    </security:authorize>

	<spring:message code="advertisement.title" var="title" />
	<display:column property="title" title="${title}" sortable="false" />
	<spring:message code="advertisement.banner" var="banner" />
	<display:column property="banner" title="${banner}" sortable="false" />
	<spring:message code="advertisement.displayDays" var="displayDays" />
	<display:column property="displayDays" title="${displayDays}" sortable="false" />
	<spring:message code="advertisement.creator" var="creator" />
	<display:column property="creator" title="${creator}"
		sortable="true" />
    <spring:message code="advertisement.hike" var="hike" />
    <display:column property="hike" title="${hike}"
                    sortable="true" />
    <security:authorize access="hasRole('AGENT')">
        <display:column>
            <a href="advertisement/edit.do?advertisementId=${row.id}"> <spring:message code="general.edit" />
            </a>
        </display:column>

        <display:column>
            <jstl:if test="${row.hike == null}">
            <a  href="advertisement/associate.do?advertisementId=${row.id}"> <spring:message code="ad.associate"/></a>
            </jstl:if>
        </display:column>
    </security:authorize>
</display:table>
