
package converters;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import org.springframework.core.convert.converter.Converter;

public class Date2ToStringConverter implements Converter<Date, String> {

	@Override
	public String convert(final Date date) {
		String result;
		final SimpleDateFormat myFormat = new SimpleDateFormat("dd-MM-yyy", Locale.ENGLISH);

		if (date == null)
			result = null;
		else
			//	         result = date.toString().replace("-", "/");

			result = myFormat.format(date);

		return result;
	}
}
