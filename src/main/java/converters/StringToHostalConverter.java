/*
 * StringToInConverter.java
 * 
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package converters;

import domain.Hostal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import repositories.HostalRepository;

@Component
@Transactional
public class StringToHostalConverter implements Converter<String, Hostal> {

	@Autowired
   HostalRepository administratorRepository;


	@Override
	public Hostal convert(String text) {
		Hostal result;
		int id;

		try {
			id = Integer.valueOf(text);
			result = this.administratorRepository.findOne(id);
		} catch (final Throwable oops) {
			throw new IllegalArgumentException(oops);
		}

		return result;
	}

}
