package controllers;

import domain.Advertisement;
import domain.Agent;
import domain.Hike;
import domain.Route;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import services.*;

import javax.validation.Valid;
import java.util.*;

@Controller
@RequestMapping("/advertisement")
public class AdvertisementController extends AbstractController {

	//Services ----------------------------------------------------------------

	@Autowired
	private AdvertisementService advertisementService;
	@Autowired
   private RouteService routeService;
   @Autowired
   private AgentService agentService;
   @Autowired
   private HikeService hikeService;
   @Autowired
   private CreditCardService creditCardService;

	//Constructors----------------------------------------------

	public AdvertisementController() {
		super();
	}

   private ModelAndView createEditModelAndView(Advertisement advertisement) {
      ModelAndView result;

      result = createEditModelAndView(advertisement, null);

      return result;
   }


   private ModelAndView createEditModelAndView(Advertisement advertisement, String message) {
      ModelAndView result;

      result = new ModelAndView("advertisement/edit");
      result.addObject("advertisement", advertisement);
      Set<Route> routes = new HashSet<>(routeService.findAll());
      result.addObject("routes",routes);
      result.addObject("message", message);

      return result;

   }


	// Edition ---------------------------------------------------------


	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView advertisementList() {

		ModelAndView result;
		final Collection<Advertisement> advertisements = this.advertisementService.findAll();

		result = new ModelAndView("advertisement/list");
		result.addObject("advertisements", advertisements);
		result.addObject("requestURI", "advertisement/list.do");
		 
		return result;
	}
   @RequestMapping(value = "/listMy", method = RequestMethod.GET)
   public ModelAndView advertisementListMyFromAgent() {

      ModelAndView result;
      Agent agent = agentService.findByPrincipal();
      final Collection<Advertisement> advertisements = advertisementService.notHiddenAdsOfAgent(agent.getId());

      result = new ModelAndView("advertisement/list");
      result.addObject("advertisements", advertisements);
      result.addObject("requestURI", "advertisement/list.do");

      return result;
   }


   @RequestMapping(value = "/create", method = RequestMethod.GET)
   public ModelAndView create() {

      ModelAndView result;

       try {
           Boolean valid = creditCardService.checkCreditCard2(agentService.findByPrincipal().getCreditCard());
           if(valid){
               Advertisement advertisement = advertisementService.create();
               result = createEditModelAndView(advertisement);
               advertisement.setCreator(agentService.findByPrincipal());
           }else{
               result = new ModelAndView("administrator/error");
               result.addObject("trace", "The credit card is invalid");
           }
       } catch (Exception e) {
           result = new ModelAndView("administrator/error");
           result.addObject("trace", e.getMessage());
           return result;
       }
       return result;

   }

//	@RequestMapping(value = "/register", method = RequestMethod.POST, params = "save")
//	public ModelAndView register(@Valid final Advertisement advertisement, final BindingResult binding) {
//		ModelAndView result;
//		if (binding.hasErrors())
//			result = AdvertisementController.createEditModelAndView2(advertisement, "general.commit.error2");
//		else
//			try {
//				advertisementService.save(advertisement);
//				result = new ModelAndView("advertisement/success");
//			} catch (final Throwable oops) {
//				result = AdvertisementController.createEditModelAndView2(advertisement, "general.commit.error");
//			}
//
//		return result;
//	}

	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public ModelAndView edit(@RequestParam final int advertisementId) {
		ModelAndView result;
		Advertisement advertisement;
		advertisement = this.advertisementService.findOne(advertisementId);
		Assert.notNull(advertisement);
        Assert.isTrue(agentService.findByPrincipal().getAdvertisements().contains(advertisement));
        advertisement.setCreator(agentService.findByPrincipal());
		result = createEditModelAndView(advertisement);

		return result;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@Valid final Advertisement advertisement, final BindingResult binding) {
		ModelAndView result;
      advertisement.setCreator(agentService.findByPrincipal());
		if (binding.hasErrors())
			result = createEditModelAndView(advertisement);
		else
			try {

				this.advertisementService.save(advertisement);
				result = advertisementListMyFromAgent();
			} catch (final Throwable oops) {
				result = createEditModelAndView(advertisement, "general.commit.error");
			}
		 
		return result;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "delete")
	public ModelAndView delete(final Advertisement advertisement) {
		ModelAndView result;
		try {
			this.advertisementService.delete(advertisement);
			result = new ModelAndView("welcome/index");
		} catch (final Throwable oops) {
			result = createEditModelAndView(advertisement, "general.commit.error");
		}
		 
		return result;
	}


   @RequestMapping(value = "/associate", method = RequestMethod.GET)
   public ModelAndView associate(@RequestParam int advertisementId) {
      ModelAndView result;
      Advertisement advertisement;

      advertisement = advertisementService.findOne(advertisementId);
      Assert.notNull(advertisement);
      Assert.notNull(agentService.findByPrincipal().getAdvertisements().contains(advertisement));
      result = createEditModelAndViewAssociate(advertisement);
      result.addObject("hikes", hikeService.findAll());
      return result;
   }


   @RequestMapping(value = "/associate", method = RequestMethod.POST, params = "save")
   public ModelAndView associate(@Valid Advertisement advertisement, BindingResult binding) {
      ModelAndView result;
      if (binding.hasErrors()) {
         result = createEditModelAndViewAssociate(advertisement);
      } else {
         try {
            advertisementService.save(advertisement);
            advertisement.getHike().getAds().add(advertisement);
            result = this.advertisementListMyFromAgent();
         } catch (Throwable oops) {
            result = createEditModelAndViewAssociate(advertisement, "general.commit.error");
         }
      }
      return result;
   }

   private ModelAndView createEditModelAndViewAssociate(Advertisement advertisement) {
      ModelAndView result;

      result = createEditModelAndViewAssociate(advertisement, null);

      return result;
   }


   private ModelAndView createEditModelAndViewAssociate(Advertisement advertisement, String message) {
      ModelAndView result;

      result = new ModelAndView("advertisement/associate");
      result.addObject("advertisement", advertisement);
      result.addObject("message", message);

      return result;

   }
//	@RequestMapping(value = "/view", method = RequestMethod.GET)
//	public ModelAndView lessorViewAn(@RequestParam final int advertisementId) {
//
//		ModelAndView result;
//		final Advertisement advertisement = this.advertisementService.findOne(advertisementId);
//
//		result = new ModelAndView("advertisement/view");
//		result.addObject("title", advertisement.getTitle());
//		result.addObject("banner", advertisement.getBanner());
//		result.addObject("requestURI", "advertisement/view.do");
//		return result;
//	}
}