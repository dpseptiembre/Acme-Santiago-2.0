
package controllers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import security.UserAccountService;
import services.AmenitiesService;
import services.InnkeeperService;
import services.UserService;
import domain.Hostal;
import domain.Innkeeper;
import domain.Registration;
import domain.User;

@Controller
@RequestMapping("/user")
public class UserController extends AbstractController {

	//Services ----------------------------------------------------------------

	@Autowired
	private UserService			userService;
	@Autowired
	private UserAccountService	userAccountService;
	@Autowired
	private InnkeeperService	innkeeperService;
	@Autowired
	private AmenitiesService	amenitiesService;


	//Constructors----------------------------------------------

	public UserController() {
		super();
	}

	protected static ModelAndView createEditModelAndView(final User user) {
		ModelAndView result;

		result = UserController.createEditModelAndView(user, null);

		return result;
	}

	//Create Method -----------------------------------------------------------

	protected static ModelAndView createEditModelAndView(final User user, final String message) {
		ModelAndView result;

		result = new ModelAndView("user/edit");
		result.addObject("user", user);
		result.addObject("message", message);

		return result;

	}

	protected static ModelAndView createEditModelAndView2(final User user) {
		ModelAndView result;

		result = UserController.createEditModelAndView2(user, null);

		return result;
	}
	// Edition ---------------------------------------------------------

	protected static ModelAndView createEditModelAndView2(final User user, final String message) {
		ModelAndView result;

		result = new ModelAndView("user/register");
		result.addObject("user", user);
		result.addObject("message", message);

		return result;

	}

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView userList() {
		ModelAndView result;
		final Collection<User> users = this.userService.findAll();
		result = new ModelAndView("user/list");
		try {
			final User user = this.userService.findByPrincipal();
			if (user != null)
				users.remove(user);
			result.addObject(user);
		} finally {

			result.addObject("users", users);
			result.addObject("requestURI", "user/list.do");
			return result;
		}
	}

	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create() {

		ModelAndView result;
		User user;

		user = this.userService.create();
		result = UserController.createEditModelAndView2(user);

		return result;

	}

	@RequestMapping(value = "/register", method = RequestMethod.POST, params = "save")
	public ModelAndView register(@Valid @ModelAttribute final User user, final BindingResult binding) {
		ModelAndView result;
		if (binding.hasErrors())
			result = UserController.createEditModelAndView2(user, "general.commit.error2");
		else
			try {
				this.userService.registerAsUser(user);
				result = new ModelAndView("user/success");
			} catch (final Throwable oops) {
				result = UserController.createEditModelAndView2(user, "general.commit.error");
			}

		return result;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public ModelAndView edit(@RequestParam final int userId) {
		ModelAndView result;
		User user;
		user = this.userService.findOne(userId);
		Assert.notNull(user);
		result = UserController.createEditModelAndView(user);

		return result;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@Valid final User user, final BindingResult binding) {
		ModelAndView result;
		if (binding.hasErrors())
			result = UserController.createEditModelAndView(user);
		else
			try {
				this.userService.save(user);
				result = new ModelAndView("user/success");
			} catch (final Throwable oops) {
				result = UserController.createEditModelAndView(user, "user.commit.error");
			}

		return result;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "delete")
	public ModelAndView delete(final User user) {
		ModelAndView result;
		try {
			this.userService.delete(user);
			result = new ModelAndView("welcome/index");
		} catch (final Throwable oops) {
			result = UserController.createEditModelAndView(user, "user.commit.error");
		}

		return result;
	}

	//	@RequestMapping(value = "/view", method = RequestMethod.GET)
	//	public ModelAndView lessorViewAn(@RequestParam final int userId) {
	//
	//		ModelAndView result;
	//		final User user = this.userService.findOne(userId);
	//
	//		result = new ModelAndView("user/view");
	//		result.addObject("name", user.getName());
	//		result.addObject("surname", user.getSurname());
	//		result.addObject("email", user.getEmail());
	//		result.addObject("number", user.getPhone());
	//		result.addObject("address", user.getAddress());
	//		result.addObject("picture", user.getPicture());
	//		result.addObject("requestURI", "user/view.do");
	//		return result;
	//	}

	@RequestMapping(value = "/view", method = RequestMethod.GET)
	public ModelAndView view(@RequestParam final int userId) {
		ModelAndView result;
		try {
			User user;
			user = this.userService.findOne(userId);
			Assert.notNull(user);
			result = new ModelAndView("user/view");
			result.addObject("us", user);
		} catch (final Exception e) {
			result = new ModelAndView("administrator/error");
			result.addObject("trace", e.getMessage());
			return result;
		}

		return result;
	}

	@RequestMapping(value = "/certify", method = RequestMethod.GET)
	public ModelAndView certify() {
		final ModelAndView result;

		Innkeeper innkeeper;
		innkeeper = this.innkeeperService.findByPrincipal();
		final List<Hostal> hostals = (List<Hostal>) this.amenitiesService.findByInnkeeperId(innkeeper.getId());
		final List<Registration> registrations = new ArrayList<Registration>();
		final List<Registration> res = new ArrayList<Registration>();
		for (final Hostal hostal : hostals) {
			registrations.addAll(hostal.getRegistrations());
			for (final Registration reg : registrations)
				if (reg.getStartDate() != null)
					res.add(reg);
			registrations.removeAll(hostal.getRegistrations());
		}
		result = new ModelAndView("user/certify");
		result.addObject("registrations", res);
		result.addObject("requestURI", "user/certify.do");
		return result;
	}

	@RequestMapping(value = "/follow", method = RequestMethod.GET)
	public ModelAndView userFollow(final int userId) {
		ModelAndView result;
		User user;
		user = this.userService.findByPrincipal();
		user.getFollowing().add(this.userService.findOne(userId));
		user.setFollowing(user.getFollowing());
		final User user2 = this.userService.findOne(userId);
		user2.getFollowers().add(user);
		user2.setFollowers(user2.getFollowers());
		final Collection<User> users = this.userService.findAll();

		if (user != null)
			users.remove(user);

		result = new ModelAndView("user/list");
		result.addObject(user);
		result.addObject("users", users);
		result.addObject("requestURI", "user/list.do");
		return result;
	}

	@RequestMapping(value = "/unfollow", method = RequestMethod.GET)
	public ModelAndView userUnfollow(final int userId) {
		final ModelAndView result;
		User user;
		user = this.userService.findByPrincipal();
		user.getFollowing().remove(this.userService.findOne(userId));
		user.setFollowing(user.getFollowing());
		final User user2 = this.userService.findOne(userId);
		user2.getFollowers().remove(user);
		user2.setFollowers(user2.getFollowers());
		final Collection<User> users = this.userService.findAll();

		if (user != null)
			users.remove(user);

		result = new ModelAndView("user/list");
		result.addObject(user);
		result.addObject("users", users);
		result.addObject("requestURI", "user/list.do");
		return result;
	}

	@RequestMapping(value = "/followers", method = RequestMethod.GET)
	public ModelAndView followers() {
		ModelAndView result;

		final User user = this.userService.findByPrincipal();
		final Collection<User> users = user.getFollowers();

		result = new ModelAndView("user/followers");
		result.addObject(user);
		result.addObject("users", users);
		result.addObject("requestURI", "user/followers.do");
		return result;
	}

	@RequestMapping(value = "/following", method = RequestMethod.GET)
	public ModelAndView following() {
		ModelAndView result;

		final User user = this.userService.findByPrincipal();
		final Collection<User> users = user.getFollowing();

		result = new ModelAndView("user/following");
		result.addObject(user);
		result.addObject("users", users);
		result.addObject("requestURI", "user/following.do");
		return result;
	}
}
