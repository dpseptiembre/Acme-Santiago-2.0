
package controllers;

import java.util.Collection;
import java.util.Date;

import javax.validation.Valid;

import domain.Hostal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.HostalService;
import services.InnkeeperService;
import services.RegistrationService;
import services.UserService;
import domain.Registration;

@Controller
@RequestMapping("/registration")
public class RegistrationController extends AbstractController {

	@Autowired
	private RegistrationService	registrationService;
	@Autowired
	private UserService			userService;
	@Autowired
	private HostalService hostalService;
	@Autowired
	private InnkeeperService innkeeperService;


	public RegistrationController() {
		super();
	}

	protected static ModelAndView createEditModelAndView(final Registration registration) {
		ModelAndView result;

		result = RegistrationController.createEditModelAndView(registration, null);

		return result;
	}

	protected static ModelAndView createEditModelAndView(final Registration registration, final String message) {
		ModelAndView result;

		result = new ModelAndView("registration/edit");
		result.addObject("registration", registration);
		result.addObject("message", message);

		return result;
	}

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView registrationsList() {

		ModelAndView result;
		Collection<Registration> registrations;
		registrations = this.registrationService.findAll();
		result = new ModelAndView("registration/list");
		result.addObject("registrations", registrations);
		result.addObject("requestURI", "registration/list.do");
		return result;
	}

	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create(@RequestParam final int userId) {

		ModelAndView result;
		Registration registration;
		final Collection<Hostal> hostals = this.hostalService.findAll();
		registration = this.registrationService.create();
		registration.setStartDate(new Date(System.currentTimeMillis() - 1000));
		registration.setHosted(this.userService.findOne(userId));
		result = RegistrationController.createEditModelAndView(registration);
		result.addObject("inns", hostals);

		return result;

	}
	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public ModelAndView edit(@RequestParam final int registrationId) {
		ModelAndView result;
		Registration registration;

		registration = this.registrationService.findOne(registrationId);
		Assert.notNull(registration);
		Assert.isTrue(innkeeperService.findByPrincipal().getHostals().contains(registration.getHostal()));
		result = RegistrationController.createEditModelAndView(registration);
		return result;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@Valid final Registration registration, final BindingResult binding) {
		ModelAndView result;
		if (binding.hasErrors())
			result = RegistrationController.createEditModelAndView(registration);
		else
			try {
				final Registration res = this.registrationService.save(registration);
				final Hostal hostal = this.hostalService.findOne(res.getHostal().getId());
				hostal.getRegistrations().add(res);
				hostal.setRegistrations(hostal.getRegistrations());
				this.hostalService.save(hostal);
				result = new ModelAndView("redirect:list.do");
			} catch (final Throwable oops) {
				result = RegistrationController.createEditModelAndView(registration, "registration.commit.error");
			}
		return result;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "delete")
	public ModelAndView delete(final Registration registration) {
		ModelAndView result;
		try {
			this.registrationService.delete(registration);
			result = new ModelAndView("redirect:list.do");
		} catch (final Throwable oops) {
			result = RegistrationController.createEditModelAndView(registration, "registration.commit.error");
		}
		return result;
	}

}
