
package controllers;

import java.util.Collection;

import javax.validation.Valid;

import domain.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.*;

@Controller
@RequestMapping("/walk")
public class WalkController extends AbstractController {

	// Services ----------------------------------------------------------------

	@Autowired
	private WalkService		walkService;
	@Autowired
	private UserService		userService;
	@Autowired
	private RouteService	routeService;
	@Autowired
	private HikeService hikeService;
	@Autowired
	private HostalService hostalService;


	// Constructors----------------------------------------------

	public WalkController() {
		super();
	}

	protected static ModelAndView createEditModelAndView(final Walk walk) {
		ModelAndView result;

		result = WalkController.createEditModelAndView(walk, null);

		return result;
	}

	protected static ModelAndView createEditModelAndView(final Walk walk, final String message) {
		ModelAndView result;

		result = new ModelAndView("walk/edit2");
		result.addObject("walk", walk);
		result.addObject("message", message);

		return result;
	}

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView list() {
		ModelAndView result;
		final User user = this.userService.findByPrincipal();
		final Collection<Walk> walks = user.getWalks();

		result = new ModelAndView("walk/list");
		result.addObject("walks", walks);
		result.addObject("requestURI", "walk/list.do");
		return result;
	}

	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create(@RequestParam final int routeId) {

		ModelAndView result;
		final Route route = this.routeService.findOne(routeId);
		final Walk walk = this.walkService.instantiateRoute(route);

		result = WalkController.createEditModelAndView(walk);

		return result;
	}

	@RequestMapping(value = "/edit2", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@Valid final Walk walk, final BindingResult binding) {
		ModelAndView result;
		if (binding.hasErrors())
			result = WalkController.createEditModelAndView(walk);
		else
			try {
				final User user = this.userService.findByPrincipal();
				final Walk w = this.walkService.save(walk);
				user.getWalks().add(w);
				user.setWalks(user.getWalks());
				this.userService.save(user);
				result = new ModelAndView("redirect:list.do");
			} catch (final Throwable oops) {
				result = WalkController.createEditModelAndView(walk, "walk.commit.error");
			}

		return result;
	}

	@RequestMapping(value = "/request", method = RequestMethod.GET)
	public ModelAndView request(final int walkId) {
		ModelAndView result;



		final Walk walk = this.walkService.findOne(walkId);
		Assert.isTrue(userService.findByPrincipal().getWalks().contains(walk));
		walk.setCompostelaStatus(Status.PENDING);

		final User user = this.userService.findByPrincipal();
		final Collection<Walk> walks = user.getWalks();

		result = new ModelAndView("walk/list");
		result.addObject("walks", walks);
		result.addObject("requestURI", "walk/list.do");
		return result;
	}

	@RequestMapping(value = "/view", method = RequestMethod.GET)
	public ModelAndView view(@RequestParam int walkId) {
		ModelAndView result;
		try {
			Walk walk;
			walk = walkService.findOne(walkId);
			result = new ModelAndView("walk/view");
			if (userService.findByPrincipal().getWalks().contains(walk)){
				result.addObject("my",true);
			}
			Collection<Hike> incompleteHikes = walkService.getIncompleteHikesGivenWalk(walk);
			Collection<Hike> completeHijes = walkService.getCompleteHikesGivenWalk(walk);
			result.addObject("wk",walk);
			result.addObject("com",completeHijes);
			result.addObject("in",incompleteHikes);

			return result;
		} catch (Exception e) {
			result = new ModelAndView("administrator/error");
			result.addObject("trace", e.getMessage());
			return result;
		}

	}











	@RequestMapping(value = "/registration", method = RequestMethod.GET)
	public ModelAndView registration(final int hikeId) {
		ModelAndView result;

		Hike hike = hikeService.findOne(hikeId);
		Assert.isTrue(userService.findByPrincipal().getRoutes().contains(hike.getRoute()));
		result = createEditModelAndView2(hike);
		result.addObject("hostals", hostalService.findAllValid());
		return result;
	}


	@RequestMapping(value = "/registrationsub", method = RequestMethod.POST, params = "save")
	public ModelAndView registrationsub(@Valid final Hike hike, final BindingResult binding) {
		ModelAndView result;
		if (binding.hasErrors())
			result = createEditModelAndView2(hike);
		else
			try {
				hike.setCompletedHike(true);
				Hike saved = this.hikeService.save(hike);

				saved.getAccommodation().getAcommodationsRegister().add(saved);
				hostalService.save(saved.getAccommodation());
					result = new ModelAndView("administrator/success");

			} catch (final Throwable oops) {
				result = createEditModelAndView2(hike, "general.commit.error");
				result.addObject("hostals", hostalService.findAllValid());
			}

		return result;
	}


	protected static ModelAndView createEditModelAndView2(Hike hike) {
		ModelAndView result;
		result = WalkController.createEditModelAndView2(hike, null);
		return result;
	}

	protected static ModelAndView createEditModelAndView2(final Hike hike, final String message) {
		ModelAndView result;

		result = new ModelAndView("walk/registration");
		result.addObject("hike", hike);
		result.addObject("message", message);

		return result;
	}

}
