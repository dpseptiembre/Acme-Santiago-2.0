
package controllers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.ChirpService;
import services.UserService;
import domain.Chirp;
import domain.User;

@Controller
@RequestMapping("/chirp")
public class ChirpController extends AbstractController {

	public ChirpController() {
		super();
	}


	@Autowired
	private ChirpService	chirpService;
	@Autowired
	private UserService		userService;


	protected static ModelAndView createEditModelAndView(final Chirp chirp) {
		ModelAndView result;

		result = ChirpController.createEditModelAndView(chirp, null);

		return result;
	}

	protected static ModelAndView createEditModelAndView(final Chirp chirp, final String message) {
		ModelAndView result;

		result = new ModelAndView("chirp/edit");
		result.addObject("chirp", chirp);
		result.addObject("message", message);

		return result;
	}

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView chirpsList() {

		ModelAndView result;
		Collection<Chirp> chirps;
		chirps = this.chirpService.findAll();
		result = new ModelAndView("chirp/list");
		result.addObject("chirps", chirps);
		result.addObject("requestURI", "chirp/list.do");
		return result;
	}

	@RequestMapping(value = "/followingChirp", method = RequestMethod.GET)
	public ModelAndView followingChirp() {
		ModelAndView result;
		final User user = this.userService.findByPrincipal();
		final Collection<User> users = user.getFollowing();
		final List<Chirp> chirps = new ArrayList<Chirp>();
		for (final User u : users)
			chirps.addAll(u.getChirps());
		result = new ModelAndView("chirp/followingChirp");
		result.addObject("chirps", chirps);
		result.addObject("requestURI", "chirp/followingChirp.do");
		return result;
	}

	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create() {

		ModelAndView result;
		final User user = this.userService.findByPrincipal();
		final Chirp chirp = this.chirpService.create();
		chirp.setOwner(user);
		chirp.setCreationMomment(new Date(System.currentTimeMillis() - 1000));
		result = ChirpController.createEditModelAndView(chirp);
		return result;
	}
	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public ModelAndView edit(@RequestParam final int chirpId) {
		ModelAndView result;
		Chirp chirp;

		chirp = this.chirpService.findOne(chirpId);
		Assert.notNull(chirp);
		Assert.isTrue(userService.findByPrincipal().getChirps().contains(chirp));
		result = ChirpController.createEditModelAndView(chirp);
		return result;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@Valid final Chirp chirp, final BindingResult binding) {
		ModelAndView result;
		if (binding.hasErrors())
			result = ChirpController.createEditModelAndView(chirp);
		else
			try {
				this.chirpService.save(chirp);
				result = new ModelAndView("redirect:list.do");
			} catch (final Throwable oops) {
				result = ChirpController.createEditModelAndView(chirp, "chirp.commit.error");
			}
		return result;
	}

}
