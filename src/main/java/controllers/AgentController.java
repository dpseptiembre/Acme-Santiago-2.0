package controllers;

import java.util.Collection;

import javax.validation.Valid;

import domain.CreditCard;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import security.UserAccountService;
import services.AgentService;
import domain.Agent;
import services.CreditCardService;

@Controller
@RequestMapping("/agent")
public class AgentController extends AbstractController{

	//Services ----------------------------------------------------------------

		@Autowired
		private AgentService			agentService;
		@Autowired
		private UserAccountService	userAccountService;
		@Autowired
      private CreditCardService creditCardService;


		//Constructors----------------------------------------------

		public AgentController() {
			super();
		}

		protected static ModelAndView createEditModelAndView(final Agent agent) {
			ModelAndView result;

			result = AgentController.createEditModelAndView(agent, null);

			return result;
		}

		//Create Method -----------------------------------------------------------

		protected static ModelAndView createEditModelAndView(final Agent agent, final String message) {
			ModelAndView result;

			result = new ModelAndView("agent/edit");
			result.addObject("agent", agent);
			result.addObject("message", message);

			return result;

		}

		protected static ModelAndView createEditModelAndView2(final Agent agent) {
			ModelAndView result;

			result = AgentController.createEditModelAndView2(agent, null);

			return result;
		}
		// Edition ---------------------------------------------------------

		protected static ModelAndView createEditModelAndView2(final Agent agent, final String message) {
			ModelAndView result;

			result = new ModelAndView("agent/register");
			result.addObject("agent", agent);
			result.addObject("message", message);

			return result;

		}


	protected static ModelAndView createEditModelAndView3(final CreditCard agent) {
		ModelAndView result;

		result = AgentController.createEditModelAndView3(agent, null);

		return result;
	}
	// Edition ---------------------------------------------------------

	protected static ModelAndView createEditModelAndView3(final CreditCard agent, final String message) {
		ModelAndView result;

		result = new ModelAndView("agent/editCreditCard");
		result.addObject("creditCard", agent);
		result.addObject("message", message);

		return result;

	}

		@RequestMapping(value = "/list", method = RequestMethod.GET)
		public ModelAndView agentList() {

			ModelAndView result;
			final Collection<Agent> agents = this.agentService.findAll();

			result = new ModelAndView("agent/list");
			result.addObject("agents", agents);
			result.addObject("requestURI", "agent/list.do");
			 
			return result;
		}

		@RequestMapping(value = "/create", method = RequestMethod.GET)
		public ModelAndView create() {

			ModelAndView result;
			Agent agent;

			agent = this.agentService.create();
			result = AgentController.createEditModelAndView2(agent);
			 
			return result;

		}

		@RequestMapping(value = "/register", method = RequestMethod.POST, params = "save")
		public ModelAndView register(@Valid final Agent agent, final BindingResult binding) {
			ModelAndView result;
			if (binding.hasErrors())
				result = createEditModelAndView2(agent, "general.commit.error");
			else
				try {

					 agentService.registerAsAgent(agent);
					result = new ModelAndView("agent/success");
				} catch (final Throwable oops) {
               result = createEditModelAndView2(agent, "general.commit.error");
				}
			 
			return result;
		}

		@RequestMapping(value = "/edit", method = RequestMethod.GET)
		public ModelAndView edit() {
			ModelAndView result;
			Agent agent;
			Agent a = agentService.findByPrincipal();
			agent = this.agentService.findOne(a.getId());
			Assert.notNull(agent);
			result = createEditModelAndView(agent);
			 
			return result;
		}

	@RequestMapping(value = "/editCreditCard", method = RequestMethod.GET)
	public ModelAndView eeditCreditCarddit() {
		ModelAndView result;
		try {

			CreditCard creditCard = agentService.findByPrincipal().getCreditCard();
			result = createEditModelAndView3(creditCard);
		} catch (Exception e) {
			result = new ModelAndView("administrator/error");
			result.addObject("trace", e.getMessage());
			return result;
		}

		return result;
	}


	@RequestMapping(value = "/saveCreditCard", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@Valid CreditCard creditCard, BindingResult binding) {
		ModelAndView result;
		if (binding.hasErrors())
			result = createEditModelAndView3(creditCard);
		else
			try {
				CreditCard c = creditCardService.save(creditCard);
				agentService.findByPrincipal().setCreditCard(c);
				result = new ModelAndView("agent/success");
			} catch (final Throwable oops) {
				result = createEditModelAndView3(creditCard, "general.commit.error");
			}

		return result;
	}


		@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
		public ModelAndView save(@Valid final Agent agent, final BindingResult binding) {
			ModelAndView result;
			if (binding.hasErrors())
				result = createEditModelAndView(agent);
			else
				try {
					this.agentService.save(agent);
					result = new ModelAndView("agent/success");
				} catch (final Throwable oops) {
					result = AgentController.createEditModelAndView(agent, "general.commit.error");
				}
			 
			return result;
		}

		@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "delete")
		public ModelAndView delete(final Agent agent) {
			ModelAndView result;
			try {
				this.agentService.delete(agent);
				result = new ModelAndView("welcome/index");
			} catch (final Throwable oops) {
				result = AgentController.createEditModelAndView(agent, "general.commit.error");
			}
			 
			return result;
		}

		@RequestMapping(value = "/view", method = RequestMethod.GET)
		public ModelAndView lessorViewAn(@RequestParam final int agentId) {

			ModelAndView result;
			final Agent agent = this.agentService.findOne(agentId);

			result = new ModelAndView("agent/view");
			result.addObject("name", agent.getName());
			result.addObject("surname", agent.getSurname());
			result.addObject("email", agent.getEmail());
			result.addObject("number", agent.getPhone());
			result.addObject("address", agent.getAddress());
			result.addObject("picture", agent.getPicture());
			result.addObject("requestURI", "agent/view.do");
			return result;
		}
	
}
