
package controllers;

import java.util.Collection;

import domain.Hostal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.AmenitiesService;
import services.HostalService;
import services.InnkeeperService;
import domain.Amenities;
import domain.Innkeeper;

@Controller
@RequestMapping("/amenities")
public class AmenitiesController extends AbstractController {

	public AmenitiesController() {
		super();
	}


	@Autowired
	private AmenitiesService	amenitiesService;
	@Autowired
	private InnkeeperService	innkeeperService;
	@Autowired
	private HostalService hostalService;


	private ModelAndView createEditModelAndView( Amenities amenities) {
		ModelAndView result;

		result = createEditModelAndView(amenities, null);

		return result;
	}

	private ModelAndView createEditModelAndView( Amenities amenities, String message) {
		ModelAndView result;

		result = new ModelAndView("amenities/edit");
		result.addObject("amenities", amenities);
		result.addObject("message", message);

		return result;
	}

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView amenitiesesList(@RequestParam int hostalId) {

		ModelAndView result;
		 Hostal hostal = this.hostalService.findOne(hostalId);
		Assert.isTrue(innkeeperService.findByPrincipal().getHostals().contains(hostal));
		Collection<Amenities> amenitieses = hostal.getAmenities();
		result = new ModelAndView("amenities/list");
		result.addObject("amenitieses", amenitieses);

		result.addObject("requestURI", "amenities/list.do");
		return result;
	}

	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create(@RequestParam int hostalId) {

		ModelAndView result;
		 Hostal hostal = this.hostalService.findOne(hostalId);
		 Amenities amenities = amenitiesService.create();
		amenities.setHostal(hostal);
		result = createEditModelAndView(amenities);
		return result;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public ModelAndView edit(@RequestParam int amenitiesId) {
		ModelAndView result;
		Amenities amenities;

		amenities = amenitiesService.findOne(amenitiesId);
		Assert.isTrue(innkeeperService.findByPrincipal().getHostals().contains(amenities.getHostal()));
		Assert.notNull(amenities);
		result = createEditModelAndView(amenities);

		return result;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView save(Amenities amenities, BindingResult binding) {
		ModelAndView result;
		if (binding.hasErrors())
			result = createEditModelAndView(amenities);
		else
			try {
			   Amenities saved = amenitiesService.save(amenities);
			   if(!saved.getHostal().getAmenities().contains(saved)){
				   saved.getHostal().getAmenities().add(saved);
				   hostalService.save(saved.getHostal());
			   }


				result = new ModelAndView("administrator/success");
			} catch (Throwable oops) {
				result = createEditModelAndView(amenities, "amenities.commit.error");
			}
		return result;
	}

	@RequestMapping(value = "/delete", method = RequestMethod.GET)
	public ModelAndView delete(@RequestParam int amenitiesId) {
		ModelAndView result;
		Innkeeper innkeeper;
		innkeeper = innkeeperService.findByPrincipal();

		try {
			 Amenities amenities = amenitiesService.findOne(amenitiesId);
			Assert.isTrue(innkeeperService.findByPrincipal().getHostals().contains(amenities.getHostal()));
			Collection<Hostal> hostals = hostalService.findAll();
			for (Hostal hostal : hostals) {
				if (hostal.getAmenities().contains(amenities))
					Assert.isTrue(innkeeper.getId() == hostal.getOwner().getId());
				amenitiesService.delete(amenities);
			}
			result = new ModelAndView("welcome/index");
		} catch (Throwable oops) {
			 Amenities amenities = amenitiesService.findOne(amenitiesId);
			result = createEditModelAndView(amenities, "amenities.commit.error");
		}
		return result;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "delete")
	public ModelAndView delete( Amenities amenities) {
		ModelAndView result;
		Innkeeper innkeeper;
		innkeeper = innkeeperService.findByPrincipal();
		 Collection<Hostal> hostals = hostalService.findAll();
		for (Hostal hostal : hostals)
			if (hostal.getAmenities().contains(amenities))
				Assert.isTrue(innkeeper.getId() == hostal.getOwner().getId());
		try {
			this.amenitiesService.delete(amenities);
			result = new ModelAndView("welcome/index");
		} catch (Throwable oops) {
			result = createEditModelAndView(amenities, "amenities.commit.error");
		}
		return result;
	}

}
