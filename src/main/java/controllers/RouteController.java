
package controllers;

import java.util.Collection;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.RouteService;
import services.UserService;
import domain.Route;
import domain.User;

@Controller
@RequestMapping("/route")
public class RouteController extends AbstractController {

	// Services ----------------------------------------------------------------

	@Autowired
	private RouteService	routeService;
	@Autowired
	private UserService		userService;


	// Constructors----------------------------------------------

	public RouteController() {
		super();
	}

	protected static ModelAndView createEditModelAndView(final Route route) {
		ModelAndView result;

		result = RouteController.createEditModelAndView(route, null);

		return result;
	}

	protected static ModelAndView createEditModelAndView(final Route route, final String message) {
		ModelAndView result;

		result = new ModelAndView("route/edit");
		result.addObject("route", route);
		result.addObject("message", message);

		return result;
	}

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView routesList() {

		ModelAndView result;
		Collection<Route> routes;
		routes = this.routeService.findAll();
		result = new ModelAndView("route/list");
		result.addObject("routes", routes);
		result.addObject("requestURI", "route/list.do");
		return result;
	}

	@RequestMapping(value = "/myList", method = RequestMethod.GET)
	public ModelAndView myList() {
		ModelAndView result;
		final User user = this.userService.findByPrincipal();
		final Collection<Route> routes = user.getRoutes();
		result = new ModelAndView("route/myList");
		result.addObject("routes", routes);
		result.addObject("requestURI", "route/myList.do");
		return result;
	}

	@RequestMapping(value = "/userList", method = RequestMethod.GET)
	public ModelAndView userList(@RequestParam final int userId) {
		ModelAndView result;
		final User user = this.userService.findOne(userId);
		final Collection<Route> routes = user.getRoutes();
		result = new ModelAndView("route/userList");
		result.addObject("routes", routes);
		result.addObject("requestURI", "route/userList.do");
		return result;
	}

	/*
	 * Return the list of routes without forms
	 */
	@RequestMapping(value = "/listInternal", method = RequestMethod.GET)
	public ModelAndView listInternal() {

		ModelAndView result;
		Collection<Route> routes;
		routes = this.routeService.findAll();
		result = new ModelAndView("route/listInternal");
		result.addObject("routes", routes);
		result.addObject("requestURI", "route/listInternal.do");
		return result;
	}

	@RequestMapping(value = "/listKeyword", method = RequestMethod.POST)
	public ModelAndView listKeyword(@RequestParam final String keyword) {

		ModelAndView result;
		Collection<Route> routes;
		try {
			routes = this.routeService.findByKeyword(keyword);
			result = new ModelAndView("route/list");
			result.addObject("requestURI", "route/listKeyword");
			result.addObject("routes", routes);
		} catch (final Throwable error) {
			result = new ModelAndView("redirect:list.do");
		}
		return result;
	}

	@RequestMapping(value = "/listByLength", method = RequestMethod.POST)
	public ModelAndView listByLength(@RequestParam  Integer keywordMin, @RequestParam Integer keywordMax ) {

		ModelAndView result;
		Collection<Route> routes;
		try {
			routes = this.routeService.listByLength(keywordMin,keywordMax);
			result = new ModelAndView("route/list");
			result.addObject("requestURI", "route/listByLength");
			result.addObject("routes", routes);
		} catch (final Throwable error) {
			result = new ModelAndView("redirect:list.do");
		}
		return result;
	}

	@RequestMapping(value = "/listMinimumHikes", method = RequestMethod.POST)
	public ModelAndView listMinimumHikes(@RequestParam final Integer keyword) {

		ModelAndView result;
		Collection<Route> routes;
		try {
			routes = this.routeService.listMinimumHikes(keyword);
			result = new ModelAndView("route/list");
			result.addObject("routes", routes);
			result.addObject("requestURI", "route/listMinimumHikes.do");
		} catch (final Throwable error) {
			result = new ModelAndView("redirect:list.do");
		}
		return result;
	}

	@RequestMapping(value = "/listMaximumHikes", method = RequestMethod.POST)
	public ModelAndView listMaximumHikes(@RequestParam final Integer keyword) {

		ModelAndView result;
		Collection<Route> routes;
		try {
			routes = this.routeService.listMaximumHikes(keyword);
			result = new ModelAndView("route/list");
			result.addObject("routes", routes);
			result.addObject("requestURI", "route/listMaximumHikes.do");
		} catch (final Throwable error) {
			result = new ModelAndView("redirect:list.do");
		}
		return result;
	}

	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create() {

		ModelAndView result;
		User user;
		user = this.userService.findByPrincipal();

		final Route route = this.routeService.create();
		route.setOwner(user);
		result = RouteController.createEditModelAndView(route);

		return result;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public ModelAndView edit(@RequestParam final int routeId) {
		ModelAndView result;
		Route route;

		route = this.routeService.findOne(routeId);
		Assert.notNull(route);
		Assert.isTrue(userService.findByPrincipal().getRoutes().contains(route));
		result = RouteController.createEditModelAndView(route);

		return result;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@Valid final Route route, final BindingResult binding) {
		ModelAndView result;
		if (binding.hasErrors())
			result = RouteController.createEditModelAndView(route);
		else
			try {

				this.routeService.save(route);
				result = new ModelAndView("redirect:myList.do");
			} catch (final Throwable oops) {
				result = RouteController.createEditModelAndView(route, "route.commit.error");
			}

		return result;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "delete")
	public ModelAndView delete(final Route route) {
		ModelAndView result;
		try {
			this.routeService.delete(route);
			result = new ModelAndView("redirect:myList.do");
		} catch (final Throwable oops) {
			result = RouteController.createEditModelAndView(route, "route.commit.error");
		}

		return result;
	}

	@RequestMapping(value = "/view", method = RequestMethod.GET)
	public ModelAndView view(@RequestParam int routeId) {
		ModelAndView result;
		try {
			Route route;
			route = this.routeService.findOne(routeId);
			Assert.notNull(route);
			result = new ModelAndView("route/view");
			result.addObject("rt",route);

			if(userService.findByPrincipal().getRoutes().contains(route)){
				result.addObject("my", true);
			}
		} catch (Exception e) {
			result = new ModelAndView("administrator/error");
			result.addObject("trace", e.getMessage());
			return result;
		}

		return result;
	}

}
