package controllers;

import domain.Agent;
import domain.Hike;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import services.AgentService;
import services.HikeService;
import services.RouteService;
import services.UserService;

import javax.validation.Valid;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

@Controller
@RequestMapping("/hike")
public class HikeController extends AbstractController {

	//Services ----------------------------------------------------------------

	@Autowired
	private HikeService hikeService;
   @Autowired
   private AgentService agentService;
   @Autowired
   private UserService userService;


	@Autowired
	private RouteService routeService;

	//Constructors----------------------------------------------

	public HikeController() {
		super();
	}

	protected static ModelAndView createEditModelAndView(final Hike hike) {
		ModelAndView result;

		result = createEditModelAndView(hike, null);

		return result;
	}

	//Create Method -----------------------------------------------------------

	protected static ModelAndView createEditModelAndView(final Hike hike, final String message) {
		ModelAndView result;

		result = new ModelAndView("hike/edit");
		result.addObject("hike", hike);
		result.addObject("message", message);

		return result;

	}



	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView hikeList() {

		ModelAndView result;
		final Collection<Hike> hikes = this.hikeService.findAll();

		result = new ModelAndView("hike/list");
		result.addObject("hikes", hikes);
		result.addObject("requestURI", "hike/list.do");
		 
		return result;
	}


   @RequestMapping(value = "/listOFAgentOnAds", method = RequestMethod.GET)
   public ModelAndView registeredHikesFromAgentOnAdvertisement() {

      ModelAndView result;
      Agent agent = agentService.findByPrincipal();
      result = new ModelAndView("hike/list");
      result.addObject("hikes", hikeService.hikesInWhichIHeveRegisteredAnAd(agent.getId()));
      result.addObject("requestURI", "hike/listOFAgentOnAds.do");

      return result;
   }
   @RequestMapping(value = "/listOFAgentWithNoAds", method = RequestMethod.GET)
   public ModelAndView registeredHikesFromAgentWithoutAdvertisement() {

      ModelAndView result;
      Agent agent = agentService.findByPrincipal();
      result = new ModelAndView("hike/list");
      result.addObject("hikes", hikeService.hikesInWhichIHeveNotRegisteredAnAd(agent.getId()));
      result.addObject("requestURI", "hike/listOFAgentWithNoAds.do");

      return result;
   }

	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create(@RequestParam int routeId) {

		ModelAndView result;
		try {
			Hike hike;
			hike = this.hikeService.create(routeId);
			result = createEditModelAndView(hike);
		} catch (Exception e) {
			result = new ModelAndView("administrator/error");
			result.addObject("trace", e.getMessage());
			return result;		}

		return result;

	}



	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public ModelAndView edit(@RequestParam final int hikeId) {
		ModelAndView result;
		Hike hike;
		hike = this.hikeService.findOne(hikeId);
		Assert.notNull(hike);
		Assert.isTrue(userService.findByPrincipal().getRoutes().contains(hike.getRoute()));
		result = createEditModelAndView(hike);
		 
		return result;
	}

	@RequestMapping(value = "/view", method = RequestMethod.GET)
	public ModelAndView view(@RequestParam int hikeId) {
		ModelAndView result;
		try {
			Hike hike;
			hike = hikeService.findOne(hikeId);
			Assert.notNull(hike);
			String ad = hikeService.randomAds(hike);
			result = new ModelAndView("hike/view");
			result.addObject("ad",ad);
			result.addObject("h",hike);
		} catch (Exception e) {
			result = new ModelAndView("administrator/error");
			result.addObject("trace", e.getMessage());
			return result;
		}

		return result;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@Valid final Hike hike, final BindingResult binding) {
		ModelAndView result;
		if (binding.hasErrors())
			result = HikeController.createEditModelAndView(hike);
		else
			try {
				Boolean check = hikeService.checkDay(hike);
				if(check){
					Hike hike1 = this.hikeService.save(hike);
					hike1.getRoute().getHikes().add(hike1);
					hike1.getRoute().setLgt(hike1.getRoute().getLgt() + hike1.getLgt());
					routeService.save(hike1.getRoute());
					result = new ModelAndView("administrator/success");
				}else {
					result = createEditModelAndView(hike, "day.error");
				}

			} catch (final Throwable oops) {
				result = createEditModelAndView(hike, "general.commit.error");
			}

		return result;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "delete")
	public ModelAndView delete(final Hike hike) {
		ModelAndView result;
		try {
			this.hikeService.delete(hike);
			result = new ModelAndView("welcome/index");
		} catch (final Throwable oops) {
			result = createEditModelAndView(hike, "hike.commit.error");
		}
		 
		return result;
	}

//	@RequestMapping(value = "/view", method = RequestMethod.GET)
//	public ModelAndView lessorViewAn(@RequestParam final int hikeId) {
//
//		ModelAndView result;
//		final Hike hike = this.hikeService.findOne(hikeId);
//
//		result = new ModelAndView("hike/view");
//		result.addObject("name", hike.getName());
//		result.addObject("origin", hike.getOrigin());
//      result.addObject("destination", hike.getDestination());
//      result.addObject("length", hike.getLgt());
//      result.addObject("level", hike.getLevel());
//      result.addObject("pictures", hike.getPictures());
//		result.addObject("requestURI", "hike/view.do");
//		return result;
//	}
}