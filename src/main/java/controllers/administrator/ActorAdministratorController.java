/*
 * ActorAdministratorController.java
 * 
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package controllers.administrator;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.util.Collection;
import java.util.Locale;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.ActorService;
import services.AdministratorService;
import services.RouteService;
import services.WalkService;
import controllers.AbstractController;
import domain.Actor;
import domain.Advertisement;
import domain.Chirp;
import domain.Comment;
import domain.Route;
import domain.User;
import domain.Utls;
import domain.Walk;

@Controller
@RequestMapping("/administrator")
public class ActorAdministratorController extends AbstractController {

	// Services ---------------------------------------------------------------

	@Autowired
	private ActorService			actorService;
	@Autowired
	private AdministratorService	administratorService;
	@Autowired
	private WalkService				walkService;
	@Autowired
	private RouteService			routeService;


	// Constructors -----------------------------------------------------------

	public ActorAdministratorController() {
		super();
	}

	// Listing ----------------------------------------------------------------

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView list() {
		ModelAndView result;
		Collection<Actor> actors;

		actors = this.actorService.findAll();
		result = new ModelAndView("actor/list");
		result.addObject("actors", actors);

		return result;
	}

	// Creation ---------------------------------------------------------------

	// Taboo ----------------------------------------------------------------

	@RequestMapping(value = "/utilsView", method = RequestMethod.GET)
	public ModelAndView utilsView() {

		ModelAndView result;
		final Utls uttils1 = this.administratorService.getUtil();

		final Collection<String> strings = uttils1.getTabooWords();
		result = new ModelAndView("administrator/utils");
		result.addObject("strings", strings);
		return result;
	}

	@RequestMapping(value = "/createTaboo", method = RequestMethod.GET)
	public ModelAndView createSpam() {

		ModelAndView result;

		try {
			result = new ModelAndView("administrator/spam");
			return result;
		} catch (final Exception e) {
			result = new ModelAndView("administrator/error");
			result.addObject("trace", e.getMessage());
			return result;
		}

	}

	@RequestMapping(value = "/addTaboo", method = RequestMethod.GET)
	public ModelAndView addSpamWord(@RequestParam final String word) {

		ModelAndView result;

		try {
			this.administratorService.addTabooWord(word);
			result = new ModelAndView("administrator/success");
			return result;
		} catch (final Exception e) {
			result = new ModelAndView("administrator/error");
			result.addObject("trace", e.getMessage());
			return result;
		}

	}

	@RequestMapping(value = "/deleteTaboo", method = RequestMethod.GET)
	public ModelAndView deleteSpam(@RequestParam final String word) {

		ModelAndView result;

		try {
			this.administratorService.removeTabooWord(word);
			result = new ModelAndView("administrator/success");
			return result;
		} catch (final Exception e) {
			result = new ModelAndView("administrator/error");
			result.addObject("trace", e.getMessage());
			return result;
		}

	}

	// Routes ----------------------------------------------------------------

	@RequestMapping(value = "/removeRoute", method = RequestMethod.GET)
	public ModelAndView removeRoute(@RequestParam final int routeId) {
		ModelAndView result;

		try {
			this.administratorService.removeRoute(routeId);
			result = new ModelAndView("administrator/success");

		} catch (final Exception e) {
			result = new ModelAndView("administrator/error");
			result.addObject("trace", e.getMessage());
		}

		return result;
	}

	// Chirps ----------------------------------------------------------------

	@RequestMapping(value = "/listTabooChirps", method = RequestMethod.GET)
	public ModelAndView listTabooChirps() {
		ModelAndView result;

		try {
			final Collection<Chirp> chirps = this.administratorService.getChirpWithTabooWords();
			result = new ModelAndView("chirp/list");
			result.addObject("chirps", chirps);
		} catch (final Exception e) {
			result = new ModelAndView("administrator/error");
			result.addObject("trace", e.getMessage());
		}

		return result;
	}

	@RequestMapping(value = "/removeChirp", method = RequestMethod.GET)
	public ModelAndView removeChirp(@RequestParam final int chirpId) {
		ModelAndView result;

		try {
			this.administratorService.removeChirp(chirpId);
			result = new ModelAndView("administrator/success");

		} catch (final Exception e) {
			result = new ModelAndView("administrator/error");
			result.addObject("trace", e.getMessage());
		}

		return result;
	}

	// Comments ----------------------------------------------------------------

	@RequestMapping(value = "/listTabooComments", method = RequestMethod.GET)
	public ModelAndView listTabooComments() {
		ModelAndView result;

		try {
			final Collection<Comment> comments = this.administratorService.getCommentsWithTabooWords();
			result = new ModelAndView("comment/list");
			result.addObject("comments", comments);
		} catch (final Exception e) {
			result = new ModelAndView("administrator/error");
			result.addObject("trace", e.getMessage());
		}

		return result;
	}

	@RequestMapping(value = "/removeComment", method = RequestMethod.GET)
	public ModelAndView removeComment(@RequestParam final int commentId) {
		ModelAndView result;

		try {
			this.administratorService.removeComment(commentId);
			result = new ModelAndView("administrator/success");

		} catch (final Exception e) {
			result = new ModelAndView("administrator/error");
			result.addObject("trace", e.getMessage());
		}

		return result;
	}

	// Advertisement ----------------------------------------------------------------

	@RequestMapping(value = "/listTabooAdvertisement", method = RequestMethod.GET)
	public ModelAndView listTabooAdvertisement() {
		ModelAndView result;

		try {
			final Collection<Advertisement> advertisements = this.administratorService.getAdvertisementWithTabooWords();
			result = new ModelAndView("advertisement/list");
			result.addObject("advertisements", advertisements);
		} catch (final Exception e) {
			result = new ModelAndView("administrator/error");
			result.addObject("trace", e.getMessage());
		}

		return result;
	}

	@RequestMapping(value = "/removeAdvertisements", method = RequestMethod.GET)
	public ModelAndView removeAdvertisements(@RequestParam final int advertisementId) {
		ModelAndView result;

		try {
			this.administratorService.removeAdvertisement(advertisementId);
			result = new ModelAndView("administrator/success");

		} catch (final Exception e) {
			result = new ModelAndView("administrator/error");
			result.addObject("trace", e.getMessage());
		}

		return result;
	}

	// Walks ----------------------------------------------------------------

	@RequestMapping(value = "/walksAwaiting", method = RequestMethod.GET)
	public ModelAndView walksAwaiting() {
		ModelAndView result;

		try {
			final Collection<Walk> walks = this.administratorService.walksWithCompostelasAwardingDecision();
			result = new ModelAndView("walk/list");
			result.addObject("walks", walks);
		} catch (final Exception e) {
			result = new ModelAndView("administrator/error");
			result.addObject("trace", e.getMessage());
		}

		return result;
	}

	@RequestMapping(value = "/approveCompostela", method = RequestMethod.GET)
	public ModelAndView approveCompostela(@RequestParam final int walkId) {
		ModelAndView result;

		try {
			this.administratorService.approveCompostela(walkId);
			result = new ModelAndView("administrator/success");

		} catch (final Exception e) {
			result = new ModelAndView("administrator/error");
			result.addObject("trace", e.getMessage());
		}

		return result;
	}

	@RequestMapping(value = "/denyCompostela", method = RequestMethod.GET)
	public ModelAndView denyCompostela(@RequestParam final int walkId) {
		ModelAndView result;

		try {
			result = new ModelAndView("walk/deny");
			result.addObject("id", walkId);

		} catch (final Exception e) {
			result = new ModelAndView("administrator/error");
			result.addObject("trace", e.getMessage());
		}

		return result;
	}

	@RequestMapping(value = "/denyConfirm", method = RequestMethod.GET)
	public ModelAndView denyConfirm(@RequestParam final String reason, @RequestParam final String walkId) {
		ModelAndView result;

		try {
			this.administratorService.denyCompostela(walkId, reason);
			result = new ModelAndView("administrator/success");

		} catch (final Exception e) {
			result = new ModelAndView("administrator/error");
			result.addObject("trace", e.getMessage());
		}

		return result;
	}

	@RequestMapping(value = "/suggest", method = RequestMethod.GET)
	public ModelAndView suggest(final int walkId) {
		ModelAndView result;

		try {
			this.administratorService.requestDecisionAboutCompostela(walkId);
			result = this.walksAwaiting();
		} catch (final Exception e) {
			result = new ModelAndView("administrator/error");
			result.addObject("trace", e.getMessage());
		}

		return result;
	}

	// Ancillary methods ------------------------------------------------------

	@RequestMapping(value = "/terms", method = RequestMethod.GET)
	public ModelAndView terms() {
		ModelAndView result;

		try {
			result = new ModelAndView("administrator/terms");

		} catch (final Exception e) {
			result = new ModelAndView("administrator/error");
			result.addObject("trace", e.getMessage());
		}

		return result;
	}

	// Dashboard ------------------------------------------------------

	@RequestMapping(value = "/dashboard", method = RequestMethod.GET)
	public ModelAndView dashboard() {

		ModelAndView result;

		try {

			final double q1 = this.administratorService.averageRouterPerUser();
			final double q2 = this.administratorService.standarDesviationOfRoutesPerUser();
			final double q3 = this.administratorService.averageOfHikesPerRoute();
			final double q4 = this.administratorService.standarDesviationOfHikesPerRoute();
			final double q5 = this.administratorService.averageOfRoutesLenght();
			final double q6 = this.administratorService.standarDesviationOfRoutesLenght();
			final double q7 = this.administratorService.averageOfHikesLenght();
			final double q8 = this.administratorService.standarDesviationOfHikesLenght();

			final double q9 = this.administratorService.averageNumberOfChirpsPerUser();
			final double q10 = this.administratorService.averageNumberOfCommentsPerRouteIncludingHikes();
			final double q11 = this.administratorService.averageNumberOfInnsManagerPerKeepeer();
			final double q12 = this.administratorService.standarDesviationOfInnsManagedPerKeeper();

			final double q13 = this.administratorService.avgNumberOfuserRegistrationsPerDay();
			final double q14 = this.administratorService.standarDesviationOfUserRegistrationPerDay();

			final Integer q15 = this.administratorService.ratioOfRoutesWithAdvertisementVersusNot();
			final Integer q16 = this.administratorService.ratioOfAdvertismentWithTabooWords();
			final Integer q17 = this.administratorService.ratioOfCompostelasWalking();
			final Integer q18 = this.administratorService.ratioOfCompostelasRequestedVsCompostelaRejected();

			final Collection<User> q19 = this.administratorService.userWhoHasPostedLessThan25AverageNumberOfChirpsPerUser();
			final Collection<User> q20 = this.administratorService.userWhoHasPostedMoreThan75AverageNumberOfChirpsPerUser();

			final Collection<Route> q21 = this.administratorService.outliner();

			result = new ModelAndView("administrator/dashboard");

			result.addObject("q1", q1);
			result.addObject("q2", q2);
			result.addObject("q3", q3);
			result.addObject("q4", q4);
			result.addObject("q5", q5);
			result.addObject("q6", q6);
			result.addObject("q7", q7);
			result.addObject("q8", q8);
			result.addObject("q9", q9);
			result.addObject("q10", q10);
			result.addObject("q11", q11);
			result.addObject("q12", q12);
			result.addObject("q13", q13);
			result.addObject("q14", q14);
			result.addObject("q15", q15);
			result.addObject("q16", q16);
			result.addObject("q17", q17);
			result.addObject("q18", q18);
			result.addObject("q19", q19);
			result.addObject("q20", q20);
			result.addObject("q21", q21);

		} catch (final Exception e) {
			result = new ModelAndView("administrator/error");
			result.addObject("trace", e.getMessage());
		}

		return result;

	}

	@RequestMapping(value = "/generate", method = RequestMethod.GET)
	public void view(@RequestParam final int walkId, final Locale locale, final HttpServletResponse response) {
		final ModelAndView result;
		try {
			this.walkService.compostelaGenPdf(walkId, locale);

			this.getPDF1(response);

		} catch (final Exception e) {
			e.printStackTrace();

		}

	}
	@RequestMapping(value = "/downloadPdf", method = RequestMethod.GET)
	public void getPDF1(final HttpServletResponse response) {
		response.setContentType("application/pdf");
		response.setHeader("Content-disposition", "attachment;filename=" + "compostela.pdf");

		try {
			final File f = new File(System.getProperty("user.home") + "/compostela.pdf");
			final FileInputStream fis = new FileInputStream(f);
			final DataOutputStream os = new DataOutputStream(response.getOutputStream());
			response.setHeader("Content-Length", String.valueOf(f.length()));
			final byte[] buffer = new byte[1024];
			int len = 0;
			while ((len = fis.read(buffer)) >= 0)
				os.write(buffer, 0, len);
			f.delete();
		} catch (final Exception e) {
			e.printStackTrace();
		}

	}
}
