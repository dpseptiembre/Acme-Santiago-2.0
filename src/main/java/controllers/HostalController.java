
package controllers;

import java.util.Collection;

import domain.Hike;
import domain.Hostal;
import domain.Status;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.AmenitiesService;
import services.HikeService;
import services.HostalService;
import services.InnkeeperService;
import domain.Innkeeper;

@Controller
@RequestMapping("/hostal")
public class HostalController extends AbstractController {

	public HostalController() {
		super();
	}


	@Autowired
	private HostalService hostalService;
	@Autowired
	private InnkeeperService	hostalkeeperService;
	@Autowired
	private AmenitiesService	amenitiesService;
	@Autowired
	private HikeService hikeService;


	protected static ModelAndView createEditModelAndView(final Hostal hostal) {
		ModelAndView result;

		result = HostalController.createEditModelAndView(hostal, null);

		return result;
	}

	protected static ModelAndView createEditModelAndView(final Hostal hostal, final String message) {
		ModelAndView result;

		result = new ModelAndView("hostal/edit");
		result.addObject("hostal", hostal);
		result.addObject("message", message);

		return result;
	}

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView hostalsList() {
		ModelAndView result;
		final Collection<Hostal> hostals;
		Innkeeper hostalkeeper;
		hostalkeeper = this.hostalkeeperService.findByPrincipal();
		hostals = this.amenitiesService.findByInnkeeperId(hostalkeeper.getId());
		result = new ModelAndView("hostal/list");
		result.addObject("hostals", hostals);
		result.addObject("requestURI", "hostal/list.do");
		return result;
	}
	
	@RequestMapping(value = "/validlist", method = RequestMethod.GET)
	public ModelAndView hostalsValidList() {
		ModelAndView result;
		final Collection<Hostal> hostals;
		hostals = this.hostalService.findAllValid();
		result = new ModelAndView("hostal/list");
		result.addObject("hostals", hostals);
		result.addObject("requestURI", "hostal/list.do");
		return result;
	}


	@RequestMapping(value = "/hikesAwarding", method = RequestMethod.GET)
	public ModelAndView hikesAwarding() {
		ModelAndView result;
		try {
			Collection<Hike> hikesAwarding = hostalService.hikesAwardingDecision(hostalkeeperService.findByPrincipal());
			result = new ModelAndView("hike/list");
			result.addObject("hikes",hikesAwarding);
			result.addObject("requestURI", "hostal/hikesAwarding.do");
		} catch (Exception e) {
			result = new ModelAndView("administrator/error");
			result.addObject("trace", e.getMessage());
			return result;		}
		return result;
	}


	@RequestMapping(value = "/approve", method = RequestMethod.GET)
	public ModelAndView approve(@RequestParam int hikeId) {
		ModelAndView result;
		try {
			Hike hike = hikeService.findOne(hikeId);
			Assert.isTrue(hostalkeeperService.findByPrincipal().getHostals().contains(hike.getAccommodation()));
			hike.setAccomodationStatus(Status.ACCEPTED);
			hikeService.save(hike);
			return new ModelAndView("administrator/success");
		} catch (Exception e) {
			result = new ModelAndView("administrator/error");
			result.addObject("trace", e.getMessage());
		}
		return result;
	}

	@RequestMapping(value = "/deny", method = RequestMethod.GET)
	public ModelAndView deny(@RequestParam int hikeId) {
		ModelAndView result;
		try {
			Hike hike = hikeService.findOne(hikeId);
			Assert.isTrue(hostalkeeperService.findByPrincipal().getHostals().contains(hike.getAccommodation()));
			hike.setAccomodationStatus(Status.DENY);
			hikeService.save(hike);
			return new ModelAndView("administrator/success");
		} catch (Exception e) {
			result = new ModelAndView("administrator/error");
			result.addObject("trace", e.getMessage());
		}
		return result;
	}


	@RequestMapping(value = "/view", method = RequestMethod.GET)
	public ModelAndView view(@RequestParam int hostalId) {
		ModelAndView result;
		try {
			Hostal hostal;
			hostal = hostalService.findOne(hostalId);
			Assert.notNull(hostal);
			result = new ModelAndView("hostal/view");
			result.addObject("in", hostal);
		} catch (Exception e) {
			result = new ModelAndView("administrator/error");
			result.addObject("trace", e.getMessage());
			return result;
		}

		return result;
	}

}
