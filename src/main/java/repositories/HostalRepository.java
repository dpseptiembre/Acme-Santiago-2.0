/*
 * ActorRepository.java
 * 
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package repositories;

import java.util.Collection;

import domain.Hike;
import domain.Hostal;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface HostalRepository extends JpaRepository<Hostal, Integer> {

	@Query("select i from Hostal i where (i.owner.creditCard.year=?1 and i.owner.creditCard.month>=?2) or (i.owner.creditCard.year>?1)")
	Collection<Hostal> findAllValid(int y, int m);


	@Query("select c from Hostal c join c.registrations rg where rg.hosted.id = ?1")
	Collection<Hostal> findInnByUser(int userId);

	// select i from Hostal i where (i.owner.creditCard.year=2018 and
	// i.owner.creditCard.month>=8) or (i.owner.creditCard.year>2018);

	@Query("select ac from Hostal c join c.acommodationsRegister ac where c.owner.id = ?1 and ac.accomodationStatus = 0 and ac.hidden = false")
	Collection<Hike> hikesAwardingDecision(int inkkeperId);
}
