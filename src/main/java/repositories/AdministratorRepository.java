/*
 * AdministratorRepository.java
 * 
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package repositories;

import domain.*;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
public interface AdministratorRepository extends JpaRepository<Administrator, Integer> {


    @Query("select c from Chirp c where c.hidden = false and (c.title LIKE CONCAT('%',:spamWord,'%') or c.description LIKE CONCAT('%',:spamWord,'%'))")
    Collection<Chirp> spamChirps(@Param("spamWord") String spamWord);

    @Query("select c from Comment c where c.hidden = false and (c.title LIKE CONCAT('%',:spamWord,'%') or c.body LIKE CONCAT('%',:spamWord,'%'))")
    Collection<Comment> spamComments(@Param("spamWord") String spamWord);




    @Query("select hk from Walk c join c.hikes hk where c.id = ?1 and hk.destination is null ")
    Collection<Hike> getHikesWithoutDestinationInGivenWalk(int walkId);






    //Dashboard

    @Query("select avg(routes.size) from User")
    double averageRouterPerUser();
    @Query("select stddev(routes.size) from User")
    double standarDesviationOfRoutesPerUser();

    @Query("select avg(hikes.size) from Route ")
    double averageOfHikesPerRoute();
    @Query("select stddev(hikes.size) from Route ")
    double standarDesviationOfHikesPerRoute();

    @Query("select avg(rt.lgt) from Route rt")
    double averageOfRoutesLenght();
    @Query("select stddev(rt.lgt) from Route rt")
    double standarDesviationOfRoutesLenght();

    @Query("select avg(hk.lgt) from Hike hk")
    double averageOfHikesLenght();
    @Query("select stddev(hk.lgt) from Hike hk")
    double standarDesviationOfHikesLenght();

    @Query("select avg(chirps.size) from User ")
    double averageNumberOfChirpsPerUser();

    @Query("select avg(r.comments.size + hk.comments.size) from Route r join r.hikes hk")
    double averageNumberOfCommentsPerRouteIncludingHikes();

    @Query("select avg(hostals.size) from Innkeeper ")
    double averageNumberOfInnsManagerPerKeepeer();

    @Query("select stddev(hostals.size) from Innkeeper ")
    double standarDesviationOfInnsManagedPerKeeper();

//TODO The outlier routes according to their lengths


//The users who have posted less than 25% the average number of chirps per user.
    @Query("select t from User t where t.chirps.size < (select avg(chirps.size) from User) * 0.25")
    Collection<User> userWhoHasPostedLessThan25AverageNumberOfChirpsPerUser();

//The users who have posted more than 75% the average number of chirps per user
    @Query("select t from User t where t.chirps.size > (select avg(chirps.size) from User) * 0.75")
    Collection<User> userWhoHasPostedMoreThan75AverageNumberOfChirpsPerUser();


// The average and the standard deviation of the number of users per day who stay in the inns

    @Query("select avg(rg.size) from Hostal c join c.registrations rg group by rg.startDate")
    double avgNumberOfuserRegistrationsPerDay();

    @Query("select stddev(rg.size) from Hostal c join c.registrations rg group by rg.startDate")
    double standarDesviationOfUserRegistrationPerDay();

//The ratio of routes that have at least one advertisement versus the routes that don?t have any.

    @Query("select c from Route c where c.advertisements.size = 0")
    Collection<Route> routesWithoutAdvertisement();
    @Query("select c from Route c where c.advertisements.size > 0")
    Collection<Route> routesWithAdvertismenet();


//The ratio of advertisements that have taboo words.
    @Query("select c from Advertisement c where c.hidden = false and (c.title LIKE CONCAT('%',:spamWord,'%'))")
    Collection<Advertisement> spamAdvertisement(@Param("spamWord") String spamWord);
    @Query("select c from Advertisement c where c.hidden = false")
    Collection<Advertisement> notSpamAdvertisement();

//The ratio of Compostela requests that are awaiting a decision versus the total number of requests.

    @Query("select c from Walk c where c.compostelaStatus = 0")
    Collection<Walk> walksAwardingDecision();
    @Query("select c from Walk c")
    Collection<Walk> allCompostelas();

//The ratio of Compostela requests that are approved versus the total number of Compostela requests that are rejected

    @Query("select c from Walk c where c.compostelaStatus = 2")
    Collection<Walk> compostelasAccepted();
    @Query("select c from Walk c where c.compostelaStatus = 1")
    Collection<Walk> compostelasDeclined();

}
