/*
 * ActorRepository.java
 * 
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package repositories;

import java.util.Collection;
import java.util.List;

import domain.Route;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RouteRepository extends JpaRepository<Route, Integer>, PagingAndSortingRepository<Route, Integer> {

	@Query("select distinct r from Route r join r.hikes rh where r.name like %?1% or r.description like %?1% or rh.name like %?1%")
	Collection<Route> findByKeyword(String keyword);

	@Query("select distinct r from Route r where r.lgt<=?1 and r.lgt>=?2")
	Collection<Route> findByLength(Integer keywordMin, Integer keywordMax);

	@Query("select distinct r from Route r where r.hikes.size<=?1")
	Collection<Route> findMaximumHikes(Integer keywordMin);

	@Query("select distinct r from Route r where r.hikes.size>=?1")
	Collection<Route> findMinimumHikes(Integer keywordMin);


	//Get all not hidden routes

	@Query("select c from Route c where c.hidden = false")
	List<Route> findAllNotNull();

	@Query("select r from Route r where r.owner=?1")
	Collection<Route> findByUserId(int userId);


}
