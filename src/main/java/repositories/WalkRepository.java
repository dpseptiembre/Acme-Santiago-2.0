/*
 * ActorRepository.java
 * 
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package repositories;

import domain.Hike;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.Walk;

import java.util.Collection;

@Repository
public interface WalkRepository extends JpaRepository<Walk, Integer> {

    @Query("select hk from Walk c join c.hikes hk where hk.hidden= false and hk.completedHike = false and c.id = ?1")
    Collection<Hike> getIncompleteHikesGivenWalk(int walkId);


    @Query("select hk from Walk c join c.hikes hk where hk.hidden= false and hk.completedHike = true and c.id = ?1")
    Collection<Hike> getCompleteHikesGivenWalk(int walkId);

}
