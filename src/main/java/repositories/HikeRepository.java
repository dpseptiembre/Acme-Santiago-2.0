/*
 * ActorRepository.java
 * 
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package repositories;

import domain.Hike;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
public interface HikeRepository extends JpaRepository<Hike, Integer> {

   @Query("select av.hike from Agent a join a.advertisements av where a.id = ?1")
   Collection<Hike> hikesInWhichIHeveRegisteredAnAd(int agentId);

   @Query("select av.hike from Agent a join a.advertisements av where a.id = ?1")
   Collection<Hike> hikesInWhichIHeveNotRegisteredAnAd(int agentId);
   

}
