/*
 * ActorRepository.java
 * 
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package repositories;

import domain.Actor;
import domain.Advertisement;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
public interface AdvertisementRepository extends JpaRepository<Advertisement, Integer> {



   @Query("select a from Advertisement a where a.creator.id = ?1 and a.hidden=false")
   Collection<Advertisement> notHiddenAdsOfAgent(int agentId);
}
