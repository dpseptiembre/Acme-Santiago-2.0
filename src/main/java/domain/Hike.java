/*
 * Administrator.java
 * 
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package domain;

import java.util.Collection;
import java.util.Date;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.Range;
import org.hibernate.validator.constraints.SafeHtml;

@Entity
@Access(AccessType.PROPERTY)
public class Hike extends DomainEntity {

	// Constructors -----------------------------------------------------------

	public Hike() {
		super();
	}


	// Attributes -------------------------------------------------------------

	private String						name;
	private int							lgt;
	private String						origin;
	private String						destination;
	private Collection<String>			pictures;
	private Level						level;
	private boolean						hidden;
	private Collection<Comment>			comments;
	private Collection<Advertisement>	ads;
	private Hostal accommodation;
	private Status accomodationStatus;
	private Date accomodationDate;
	private boolean completedHike;
	private Route route;
	private int day;

	// Relationships ----------------------------------------------------------

	@NotBlank
	@SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
	public String getName() {
		return this.name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	@NotNull
	@Range(min = 10)
	public int getLgt() {
		return this.lgt;
	}

	public void setLgt(final int lenght) {
		this.lgt = lenght;
	}

	@NotBlank
	public String getOrigin() {
		return origin;
	}

	public void setOrigin(String origin) {
		this.origin = origin;
	}

	@NotBlank
	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	@ElementCollection
	public Collection<String> getPictures() {
		return this.pictures;
	}

	public void setPictures(final Collection<String> pictures) {
		this.pictures = pictures;
	}

	public Level getLevel() {
		return this.level;
	}

	public void setLevel(final Level level) {
		this.level = level;
	}

	public boolean isHidden() {
		return this.hidden;
	}

	public void setHidden(final boolean hidden) {
		this.hidden = hidden;
	}

	@OneToMany
	public Collection<Comment> getComments() {
		return this.comments;
	}

	public void setComments(final Collection<Comment> comments) {
		this.comments = comments;
	}

	@OneToMany
	public Collection<Advertisement> getAds() {
		return this.ads;
	}

	public void setAds(final Collection<Advertisement> ads) {
		this.ads = ads;
	}

	@Override
	public String toString() {
		return "Hike{" +
				"name='" + name + '\'' +
				", lgt=" + lgt +
				", origin='" + origin + '\'' +
				", destination='" + destination + '\'' +
				", level=" + level +
				'}';
	}

	@ManyToOne
	public Route getRoute() {
		return route;
	}

	public void setRoute(Route route) {
		this.route = route;
	}

	@NotNull
	@Range(min = 0, max = 500)
	public int getDay() {
		return day;
	}

	public void setDay(int day) {
		this.day = day;
	}

	@ManyToOne
	public Hostal getAccommodation() {
		return accommodation;
	}

	public void setAccommodation(Hostal accommodation) {
		this.accommodation = accommodation;
	}


	public Status getAccomodationStatus() {
		return accomodationStatus;
	}

	public void setAccomodationStatus(Status accomodationStatus) {
		this.accomodationStatus = accomodationStatus;
	}

	@NotNull
	public boolean isCompletedHike() {
		return completedHike;
	}

	public void setCompletedHike(boolean completedHike) {
		this.completedHike = completedHike;
	}

	@Temporal(TemporalType.DATE)
	public Date getAccomodationDate() {
		return accomodationDate;
	}

	public void setAccomodationDate(Date accomodationDate) {
		this.accomodationDate = accomodationDate;
	}
}
