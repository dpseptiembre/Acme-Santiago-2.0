/*
 * Administrator.java
 * 
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package domain;

import java.util.Collection;
import java.util.Date;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.SafeHtml;

@Entity
@Access(AccessType.PROPERTY)
public class Walk extends DomainEntity {

	// Constructors -----------------------------------------------------------

	public Walk() {
		super();
	}


	// Attributes -------------------------------------------------------------

	private String				name, denyReason;
	private Status				compostelaStatus;
	private Collection<Hike>	hikes;
	private Collection<Comment>	comments;
	private User				author;
	private Date				compostelaStatusChangeMomment;
	private Status				sugestedStatus;


	// Relationships ----------------------------------------------------------

	@NotBlank
	@SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
	public String getName() {
		return this.name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	@SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
	public String getDenyReason() {
		return this.denyReason;
	}

	public void setDenyReason(final String denyReason) {
		this.denyReason = denyReason;
	}

	public Status getCompostelaStatus() {
		return this.compostelaStatus;
	}

	public void setCompostelaStatus(final Status compostelaStatus) {
		this.compostelaStatus = compostelaStatus;
	}

	@Temporal(TemporalType.DATE)
	public Date getCompostelaStatusChangeMomment() {
		return this.compostelaStatusChangeMomment;
	}

	public void setCompostelaStatusChangeMomment(final Date compostelaStatusChangeMomment) {
		this.compostelaStatusChangeMomment = compostelaStatusChangeMomment;
	}

	@OneToMany
	public Collection<Hike> getHikes() {
		return this.hikes;
	}

	public void setHikes(final Collection<Hike> hikes) {
		this.hikes = hikes;
	}

	@OneToMany
	public Collection<Comment> getComments() {
		return this.comments;
	}

	public void setComments(final Collection<Comment> comments) {
		this.comments = comments;
	}

	@ManyToOne
	public User getAuthor() {
		return this.author;
	}

	public void setAuthor(final User author) {
		this.author = author;
	}

	public Status getSugestedStatus() {
		return this.sugestedStatus;
	}

	public void setSugestedStatus(final Status sugestedStatus) {
		this.sugestedStatus = sugestedStatus;
	}
}
