/*
 * Administrator.java
 * 
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package domain;

import java.util.Collection;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.SafeHtml;
import org.hibernate.validator.constraints.URL;

@Entity
@Access(AccessType.PROPERTY)
public class Hostal extends DomainEntity {

	// Constructors -----------------------------------------------------------

	public Hostal() {
		super();
	}


	// Attributes -------------------------------------------------------------

	private String						name, badge, street, number, city, phoneNumber, email, web;
	private Innkeeper					owner;
	private Collection<Amenities>		amenities;
	private Collection<Registration>	registrations;
	private Collection<Hike> acommodationsRegister;


	// Relationships ----------------------------------------------------------

	@NotBlank
	@SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
	public String getName() {
		return this.name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	@NotBlank
	@URL
	@SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
	public String getBadge() {
		return this.badge;
	}

	public void setBadge(final String badge) {
		this.badge = badge;
	}

	@NotBlank
	@SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
	public String getStreet() {
		return this.street;
	}

	public void setStreet(final String street) {
		this.street = street;
	}

	@NotBlank
	@SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
	public String getNumber() {
		return this.number;
	}

	public void setNumber(final String number) {
		this.number = number;
	}

	@NotBlank
	@SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
	public String getCity() {
		return this.city;
	}

	public void setCity(final String city) {
		this.city = city;
	}

	@NotBlank
	@SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
	public String getPhoneNumber() {
		return this.phoneNumber;
	}

	public void setPhoneNumber(final String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	@Email
	@SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
	public String getEmail() {
		return this.email;
	}

	public void setEmail(final String email) {
		this.email = email;
	}

	@URL
	@SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
	public String getWeb() {
		return this.web;
	}

	public void setWeb(final String web) {
		this.web = web;
	}

	@ManyToOne
	public Innkeeper getOwner() {
		return this.owner;
	}

	public void setOwner(final Innkeeper owner) {
		this.owner = owner;
	}

	@OneToMany
	public Collection<Amenities> getAmenities() {
		return this.amenities;
	}

	public void setAmenities(final Collection<Amenities> amenities) {
		this.amenities = amenities;
	}

	@OneToMany
	public Collection<Registration> getRegistrations() {
		return this.registrations;
	}

	public void setRegistrations(final Collection<Registration> registrations) {
		this.registrations = registrations;
	}

	@OneToMany
	public Collection<Hike> getAcommodationsRegister() {
		return acommodationsRegister;
	}

	public void setAcommodationsRegister(Collection<Hike> acommodationsRegister) {
		this.acommodationsRegister = acommodationsRegister;
	}

	@Override
	public String toString() {
		return "Hostal{" +
				"name='" + name + '\'' +
				'}';
	}
}
