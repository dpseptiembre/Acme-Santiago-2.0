/*
 * Administrator.java
 * 
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package domain;

import java.util.Collection;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.SafeHtml;
import org.hibernate.validator.constraints.URL;

@Entity
@Access(AccessType.PROPERTY)
public class Advertisement extends DomainEntity {

	// Constructors -----------------------------------------------------------

	public Advertisement() {
		super();
	}


	// Attributes -------------------------------------------------------------

	private String				title, banner;
	private int					displayDays;
	private Agent				creator;
	private boolean				hidden;
	private Collection<Route>	routes;
	private Hike				hike;


	// Relationships ----------------------------------------------------------

	@NotBlank
	@SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
	public String getTitle() {
		return this.title;
	}

	public void setTitle(final String title) {
		this.title = title;
	}

	@NotBlank
	@URL
	@SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
	public String getBanner() {
		return this.banner;
	}

	public void setBanner(final String banner) {
		this.banner = banner;
	}

	@NotNull
	public int getDisplayDays() {
		return this.displayDays;
	}

	public void setDisplayDays(final int displayDays) {
		this.displayDays = displayDays;
	}

	@ManyToOne(targetEntity = Agent.class)
	public Agent getCreator() {
		return this.creator;
	}

	public void setCreator(final Agent creator) {
		this.creator = creator;
	}

	public boolean isHidden() {
		return this.hidden;
	}

	public void setHidden(final boolean hidden) {
		this.hidden = hidden;
	}

	@ManyToMany
	public Collection<Route> getRoutes() {
		return this.routes;
	}

	public void setRoutes(final Collection<Route> routes) {
		this.routes = routes;
	}

	@ManyToOne
	public Hike getHike() {
		return this.hike;
	}

	public void setHike(final Hike hike) {
		this.hike = hike;
	}
}
