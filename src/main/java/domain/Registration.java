
package domain;

import java.util.Date;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Access(AccessType.PROPERTY)
public class Registration extends DomainEntity {

	public Registration() {
		super();
	}


	private Hostal hostal;
	private Date	startDate, endDate;
	private User	hosted;


	@ManyToOne
	public Hostal getHostal() {
		return this.hostal;
	}

	public void setHostal(final Hostal hostal) {
		this.hostal = hostal;
	}

	@Temporal(TemporalType.DATE)
	public Date getStartDate() {
		return this.startDate;
	}

	public void setStartDate(final Date startDate) {
		this.startDate = startDate;
	}

	@Temporal(TemporalType.DATE)
	public Date getEndDate() {
		return this.endDate;
	}

	public void setEndDate(final Date endDate) {
		this.endDate = endDate;
	}

	@ManyToOne
	public User getHosted() {
		return this.hosted;
	}

	public void setHosted(final User hosted) {
		this.hosted = hosted;
	}
}
