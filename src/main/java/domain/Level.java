package domain;

public enum Level {

    EASY, MEDIUM, DIFFICULT
}
