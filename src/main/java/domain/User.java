/*
 * Administrator.java
 * 
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package domain;

import java.io.Serializable;
import java.util.Collection;
import java.util.Set;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;

@Entity
@Access(AccessType.PROPERTY)
public class User extends Actor implements Serializable {

	private static final long	serialVersionUID	= 1L;


	// Constructors -----------------------------------------------------------

	public User() {
		super();
	}


	// Attributes -------------------------------------------------------------

	private Set<User>					followers;
	private Set<User>					following;
	private Collection<Route>			routes;
	private Collection<Comment>			comments;
	private Collection<Chirp>			chirps;
	private Collection<Registration>	registrations;
	private Collection<Walk>			walks;


	// Relationships ----------------------------------------------------------

	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(joinColumns = @JoinColumn(name = "followers"), inverseJoinColumns = @JoinColumn(name = "following"))
	public Set<User> getFollowers() {
		return this.followers;
	}

	public void setFollowers(final Set<User> followers) {
		this.followers = followers;
	}

	public void addFollower(final User follower) {
		this.followers.add(follower);
		follower.following.add(this);
	}

	@ManyToMany(mappedBy = "followers")
	public Set<User> getFollowing() {
		return this.following;
	}

	public void setFollowing(final Set<User> following) {
		this.following = following;
	}

	public void addFollowing(final User followed) {
		followed.addFollower(this);
	}
	@OneToMany(mappedBy = "owner")
	public Collection<Route> getRoutes() {
		return this.routes;
	}

	public void setRoutes(final Collection<Route> routes) {
		this.routes = routes;
	}

	@OneToMany(mappedBy = "creator")
	public Collection<Comment> getComments() {
		return this.comments;
	}

	public void setComments(final Collection<Comment> comments) {
		this.comments = comments;
	}

	@OneToMany(mappedBy = "owner")
	public Collection<Chirp> getChirps() {
		return this.chirps;
	}

	public void setChirps(final Collection<Chirp> chirps) {
		this.chirps = chirps;
	}

	@OneToMany
	public Collection<Registration> getRegistrations() {
		return this.registrations;
	}

	public void setRegistrations(final Collection<Registration> registrations) {
		this.registrations = registrations;
	}

	@OneToMany
	public Collection<Walk> getWalks() {
		return this.walks;
	}

	public void setWalks(final Collection<Walk> walks) {
		this.walks = walks;
	}
}
