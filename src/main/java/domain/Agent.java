/*
 * Administrator.java
 * 
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package domain;

import javax.persistence.*;
import java.util.Collection;

@Entity
@Access(AccessType.PROPERTY)
public class Agent extends Actor {

	// Constructors -----------------------------------------------------------

	public Agent() {
		super();
	}

	// Attributes -------------------------------------------------------------

	private Collection<Advertisement> advertisements;
	private CreditCard creditCard;

	// Relationships ----------------------------------------------------------


	@OneToMany(mappedBy = "creator")
	public Collection<Advertisement> getAdvertisements() {
		return advertisements;
	}

	public void setAdvertisements(Collection<Advertisement> advertisements) {
		this.advertisements = advertisements;
	}

	@OneToOne
	public CreditCard getCreditCard() {
		return creditCard;
	}

	public void setCreditCard(CreditCard creditCard) {
		this.creditCard = creditCard;
	}

   @Override
   public String toString() {
      return super.getName() + "" + super.getSurname() + "[ "+super.getEmail() + " ]";
   }
}
