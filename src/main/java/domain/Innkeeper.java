/*
 * Administrator.java
 * 
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package domain;

import javax.persistence.*;
import java.util.Collection;

@Entity
@Access(AccessType.PROPERTY)
public class Innkeeper extends Actor {

	// Constructors -----------------------------------------------------------

	public Innkeeper() {
		super();
	}

	// Attributes -------------------------------------------------------------

	private Collection<Hostal> hostals;
	private CreditCard creditCard;


	// Relationships ----------------------------------------------------------


	@OneToMany(mappedBy = "owner")
	public Collection<Hostal> getHostals() {
		return hostals;
	}

	public void setHostals(Collection<Hostal> hostals) {
		this.hostals = hostals;
	}

	@OneToOne
	public CreditCard getCreditCard() {
		return creditCard;
	}

	public void setCreditCard(CreditCard creditCard) {
		this.creditCard = creditCard;
	}
}
