/*
 * AgentService.java
 * 
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package services;

import domain.Actor;
import domain.Agent;
import domain.CreditCard;
import domain.User;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import repositories.AgentRepository;
import security.Authority;
import security.LoginService;
import security.UserAccount;
import security.UserAccountService;

import java.util.ArrayList;
import java.util.Collection;

@Service
@Transactional
public class AgentService {

	// Managed repository -----------------------------------------------------

	@Autowired
	private AgentRepository		agentRepository;

	// Supporting services ----------------------------------------------------

	@Autowired
	private UserAccountService	userAccountService;
	@Autowired
   private CreditCardService creditCardService;


	// Constructors -----------------------------------------------------------

	public AgentService() {
		super();
	}

	// Simple CRUD methods ----------------------------------------------------
	
	public Agent create() {
		final Agent res = new Agent();
		this.authoritySet(res);
		return res;

	}
	
	public Collection<Agent> findAll() {
		Collection<Agent> result;

		result = agentRepository.findAll();
		Assert.notNull(result);

		return result;
	}

	public Agent findOne(int agentId) {
		Assert.isTrue(agentId != 0);

		Agent result;

		result = agentRepository.findOne(agentId);
		Assert.notNull(result);

		return result;
	}

	public Agent save(Agent agent) {
		Assert.notNull(agent);

		Agent result;

		result = agentRepository.save(agent);

		return result;
	}

	public void delete(Agent agent) {
		Assert.notNull(agent);
		Assert.isTrue(agent.getId() != 0);
		Assert.isTrue(agentRepository.exists(agent.getId()));

		agentRepository.delete(agent);
	}

	// Other business methods -------------------------------------------------

//	public UserAccount findUserAccount(Agent agent) {
//		Assert.notNull(agent);
//
//		UserAccount result;
//
//		result = userAccountService.findByAgent(agent);
//
//		return result;
//	}

   public Agent findByPrincipal() {
      Agent result;
      UserAccount userAccount;
      userAccount = LoginService.getPrincipal();
      Assert.notNull(userAccount);
      result = findByUserAccount(userAccount);
      Assert.notNull(result);
      return result;
   }

   public Agent findByUserAccount(UserAccount userAccount) {
      Assert.notNull(userAccount);

      Agent result;

      result = agentRepository.findByUserAccountId(userAccount.getId());

      return result;
   }
   
   public Agent registerAsAgent(final Agent a) {
	   Assert.notNull(a);
		a.getUserAccount().setUsername(a.getUserAccount().getUsername());
		Md5PasswordEncoder encoder;
		encoder = new Md5PasswordEncoder();
		final String hash = encoder.encodePassword(a.getUserAccount().getPassword(), null);
		a.getUserAccount().setPassword(hash);
		final UserAccount userAccount = this.userAccountService.save(a.getUserAccount());
		a.setUserAccount(userAccount);
      final Authority autoh = new Authority();
      autoh.setAuthority(Authority.AGENT);
      final Collection<Authority> authorities = new ArrayList<>();
      authorities.add(autoh);
      final UserAccount res1 = new UserAccount();
      res1.setAuthorities(authorities);
      //final UserAccount userAccount = this.userAccountService.save(res1);
		Assert.notNull(a.getUserAccount().getAuthorities(), "authorities null al registrar");
		final Agent resu = this.agentRepository.save(a);
		return resu;
	}

	private void authoritySet(final Agent agent) {
		Assert.notNull(agent);
		final Authority autoh = new Authority();
		autoh.setAuthority(Authority.AGENT);
		final Collection<Authority> authorities = new ArrayList<>();
		authorities.add(autoh);
		final UserAccount res1 = new UserAccount();
		res1.setAuthorities(authorities);
		//final UserAccount userAccount = this.userAccountService.save(res1);
		agent.setUserAccount(res1);
		Assert.notNull(agent.getUserAccount().getAuthorities(), "authorities null al registrar");
	}
}
