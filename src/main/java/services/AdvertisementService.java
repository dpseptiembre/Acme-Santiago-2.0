/*
 * AdvertisementService.java
 * 
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package services;

import domain.Advertisement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import repositories.AdvertisementRepository;
import security.UserAccountService;

import java.util.Collection;

@Service
@Transactional
public class AdvertisementService {

	// Managed repository -----------------------------------------------------

	@Autowired
	private AdvertisementRepository		advertisementRepository;

	// Supporting services ----------------------------------------------------


	// Constructors -----------------------------------------------------------

	public AdvertisementService() {
		super();
	}

	// Simple CRUD methods ----------------------------------------------------

	public Advertisement create(){
		Advertisement result;
		result = new Advertisement();
		return result;
	}
	
	public Collection<Advertisement> findAll() {
		Collection<Advertisement> result;

		result = advertisementRepository.findAll();
		Assert.notNull(result);

		return result;
	}

	public Advertisement findOne(int advertisementId) {
		Assert.isTrue(advertisementId != 0);

		Advertisement result;

		result = advertisementRepository.findOne(advertisementId);
		Assert.notNull(result);

		return result;
	}

	public Advertisement save(Advertisement advertisement) {
		Assert.notNull(advertisement);

		Advertisement result;

		result = advertisementRepository.save(advertisement);

		return result;
	}

	public void delete(Advertisement advertisement) {
		Assert.notNull(advertisement);
		Assert.isTrue(advertisement.getId() != 0);
		Assert.isTrue(advertisementRepository.exists(advertisement.getId()));

		advertisementRepository.delete(advertisement);
	}

	// Other business methods -------------------------------------------------


   public Collection<Advertisement> notHiddenAdsOfAgent(int agentId){
	   return advertisementRepository.notHiddenAdsOfAgent(agentId);
   }
}
