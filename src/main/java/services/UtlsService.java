/*
 * UtlsService.java
 * 
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package services;

import domain.Utls;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import repositories.UtlsRepository;

import java.util.Collection;

@Service
@Transactional
public class UtlsService {

	// Managed repository -----------------------------------------------------

	@Autowired
	private UtlsRepository		utlsRepository;

	// Supporting services ----------------------------------------------------


	// Constructors -----------------------------------------------------------

	public UtlsService() {
		super();
	}

	// Simple CRUD methods ----------------------------------------------------

	public Utls create(){
		Utls result;
		result = new Utls();
		return result;
	}
	
	public Collection<Utls> findAll() {
		Collection<Utls> result;

		result = utlsRepository.findAll();
		Assert.notNull(result);

		return result;
	}

	public Utls findOne(int utlsId) {
		Assert.isTrue(utlsId != 0);

		Utls result;

		result = utlsRepository.findOne(utlsId);
		Assert.notNull(result);

		return result;
	}

	public Utls save(Utls utls) {
		Assert.notNull(utls);

		Utls result;

		result = utlsRepository.save(utls);

		return result;
	}

	public void delete(Utls utls) {
		Assert.notNull(utls);
		Assert.isTrue(utls.getId() != 0);
		Assert.isTrue(utlsRepository.exists(utls.getId()));

		utlsRepository.delete(utls);
	}

	// Other business methods -------------------------------------------------


}
