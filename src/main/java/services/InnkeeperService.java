/*
 * InnkeeperService.java
 * 
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package services;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import repositories.InnkeeperRepository;
import security.LoginService;
import security.UserAccount;
import domain.Innkeeper;

@Service
@Transactional
public class InnkeeperService {

	// Managed repository -----------------------------------------------------

	@Autowired
	private InnkeeperRepository	innkeeperRepository;


	// Supporting services ----------------------------------------------------

	// Constructors -----------------------------------------------------------

	public InnkeeperService() {
		super();
	}

	// Simple CRUD methods ----------------------------------------------------

	public Innkeeper create() {
		Innkeeper result;
		result = new Innkeeper();
		return result;
	}

	public Collection<Innkeeper> findAll() {
		Collection<Innkeeper> result;

		result = this.innkeeperRepository.findAll();
		Assert.notNull(result);

		return result;
	}

	public Innkeeper findOne(final int innkeeperId) {
		Assert.isTrue(innkeeperId != 0);

		Innkeeper result;

		result = this.innkeeperRepository.findOne(innkeeperId);
		Assert.notNull(result);

		return result;
	}

	public Innkeeper save(final Innkeeper innkeeper) {
		Assert.notNull(innkeeper);

		Innkeeper result;

		result = this.innkeeperRepository.save(innkeeper);

		return result;
	}

	public void delete(final Innkeeper innkeeper) {
		Assert.notNull(innkeeper);
		Assert.isTrue(innkeeper.getId() != 0);
		Assert.isTrue(this.innkeeperRepository.exists(innkeeper.getId()));

		this.innkeeperRepository.delete(innkeeper);
	}

	// Other business methods -------------------------------------------------

	public Innkeeper findByPrincipal() {
		Innkeeper result;
		UserAccount userAccount;
		userAccount = LoginService.getPrincipal();
		Assert.notNull(userAccount);
		result = this.findByUserAccount(userAccount);
		Assert.notNull(result);
		return result;
	}

	public Innkeeper findByUserAccount(final UserAccount userAccount) {
		Assert.notNull(userAccount);

		Innkeeper result;

		result = this.innkeeperRepository.findByUserAccountId(userAccount.getId());

		return result;
	}

}
