/*
 * RouteService.java
 * 
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package services;


import domain.Route;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import repositories.RouteRepository;

import java.util.ArrayList;
import java.util.Collection;
@Service
@Transactional
public class RouteService {

	// Managed repository -----------------------------------------------------

	@Autowired
	private RouteRepository		routeRepository;

	// Supporting services ----------------------------------------------------



	// Constructors -----------------------------------------------------------

	public RouteService() {
		super();
	}

	// Simple CRUD methods ----------------------------------------------------

	public Route create(){
		Route result;
		result = new Route();
		result.setLgt(0);
		result.setHidden(false);
		return result;
	}
	
	public Collection<Route> findAll() {
		Collection<Route> result;
		result = routeRepository.findAllNotNull();
		return result;
	}

	public Route findOne(int routeId) {
		Assert.isTrue(routeId != 0);

		Route result;

		result = routeRepository.findOne(routeId);
		Assert.notNull(result);

		return result;
	}

	public Route save(Route route) {
		Assert.notNull(route);

		Route result;

		result = routeRepository.save(route);

		return result;
	}

	public void delete(Route route) {
		Assert.notNull(route);
		Assert.isTrue(route.getId() != 0);
		Assert.isTrue(routeRepository.exists(route.getId()));

		routeRepository.delete(route);
	}


	// Other business methods -------------------------------------------------
	public Collection<Route> findByKeyword(String keyword) {
		Collection<Route> result;
		result = routeRepository.findByKeyword(keyword);
		
		return result;
	}

	public Collection<Route> listByLength(Integer keyword, Integer keywordMin) {
		Collection<Route> result;
		result= routeRepository.findByLength(keywordMin,keyword);
		return result;
	}

	public Collection<Route> listMaximumHikes(Integer keyword) {
		Collection<Route> result;
		result= routeRepository.findMaximumHikes(keyword);
		return result;
	}

	public Collection<Route> listMinimumHikes(Integer keyword) {
		Collection<Route> result;
		result= routeRepository.findMinimumHikes(keyword);
		return result;
	}


}
