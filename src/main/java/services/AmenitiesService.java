/*
 * AmenitiesService.java
 * 
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package services;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import repositories.AmenitiesRepository;
import domain.Amenities;
import domain.Hostal;

@Service
@Transactional
public class AmenitiesService {

	// Managed repository -----------------------------------------------------

	@Autowired
	private AmenitiesRepository	amenitiesRepository;


	// Supporting services ----------------------------------------------------

	// Constructors -----------------------------------------------------------

	public AmenitiesService() {
		super();
	}

	// Simple CRUD methods ----------------------------------------------------

	public Amenities create() {
		Amenities result;
		result = new Amenities();
		return result;
	}

	public Collection<Amenities> findAll() {
		Collection<Amenities> result;

		result = this.amenitiesRepository.findAll();
		Assert.notNull(result);

		return result;
	}

	public Amenities findOne(final int amenitiesId) {
		Assert.isTrue(amenitiesId != 0);

		Amenities result;

		result = this.amenitiesRepository.findOne(amenitiesId);
		Assert.notNull(result);

		return result;
	}

	public Amenities save(final Amenities amenities) {
		Assert.notNull(amenities);

		Amenities result;

		result = this.amenitiesRepository.save(amenities);

		return result;
	}

	public void delete(final Amenities amenities) {
		Assert.notNull(amenities);
		Assert.isTrue(amenities.getId() != 0);
		Assert.isTrue(this.amenitiesRepository.exists(amenities.getId()));

		this.amenitiesRepository.delete(amenities);
	}

	// Other business methods -------------------------------------------------

	public Collection<Hostal> findByInnkeeperId(final int innkeeperId) {
		Collection<Hostal> result;
		result = this.amenitiesRepository.findByInnkeeperId(innkeeperId);
		return result;
	}

}
