/*
 * WalkService.java
 * 
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package services;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import repositories.WalkRepository;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;

import domain.Hike;
import domain.Hostal;
import domain.Route;
import domain.Walk;

@Service
@Transactional
public class WalkService {

	// Managed repository -----------------------------------------------------

	@Autowired
	private WalkRepository	walkRepository;

	// Supporting services ----------------------------------------------------

	@Autowired
	private UserService		userService;
	@Autowired
	private HostalService	hostalService;


	// Constructors -----------------------------------------------------------

	public WalkService() {
		super();
	}

	// Simple CRUD methods ----------------------------------------------------

	public Walk create() {
		Walk result;
		result = new Walk();
		return result;
	}

	public Collection<Walk> findAll() {
		Collection<Walk> result;

		result = this.walkRepository.findAll();
		Assert.notNull(result);

		return result;
	}

	public Walk findOne(final int walkId) {
		Assert.isTrue(walkId != 0);

		Walk result;

		result = this.walkRepository.findOne(walkId);
		Assert.notNull(result);

		return result;
	}

	public Walk save(final Walk walk) {
		Assert.notNull(walk);

		Walk result;

		result = this.walkRepository.save(walk);

		return result;
	}

	public void delete(final Walk walk) {
		Assert.notNull(walk);
		Assert.isTrue(walk.getId() != 0);
		Assert.isTrue(this.walkRepository.exists(walk.getId()));

		this.walkRepository.delete(walk);
	}

	// Other business methods -------------------------------------------------

	public Walk instantiateRoute(final Route route) {
		final List<Hike> hikes = (List<Hike>) route.getHikes();
		final Walk walk = this.create();
		walk.setHikes(hikes);
		walk.setComments(route.getComments());
		walk.setAuthor(route.getOwner());
		return walk;
	}

	public void compostelaGenPdf(final int walkId, final Locale locale) {
		final Walk walk = this.walkRepository.findOne(walkId);
		try {
			final Document document = new Document();

			PdfWriter.getInstance(document, new FileOutputStream(System.getProperty("user.home") + "/compostela.pdf"));

			document.open();
			final Image img = Image.getInstance(new URL("https://upload.wikimedia.org/wikipedia/commons/thumb/7/78/Cruz_de_Santiago_de_Compostela.svg/744px-Cruz_de_Santiago_de_Compostela.svg.png"));
			img.scaleToFit(80, 600);
			img.setAlignment(Element.ALIGN_RIGHT);
			document.add(img);

			final Font font = FontFactory.getFont(FontFactory.COURIER, 12, BaseColor.BLACK);

			//HEADER
			final String name = walk.getAuthor().getName() + " " + walk.getAuthor().getSurname();
			final SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
			final String date = myFormat.format(walk.getCompostelaStatusChangeMomment());

			//BODY
			final String userInfo = walk.getAuthor().getEmail() + " " + walk.getAuthor().getPhone();
			final List<Walk> walks = new ArrayList<>(walk.getAuthor().getWalks());
			final List<Hostal> hostals = new ArrayList<>(this.hostalService.innsByUser(walk.getAuthor().getId()));

			final List<String> logos = new ArrayList<>();
			logos.add("https://gitlab.com/dpseptiembre/Acme_QA/raw/3229d1eefdc114bd60415dffc442914a4b2b2fa8/src/main/webapp/images/logo.png");
			logos.add("https://gitlab.com/dpseptiembre/Acme-Inmigrant/raw/master/src/main/webapp/images/logo.png");
			logos.add("https://gitlab.com/dpseptiembre/Acme_Antenna/raw/master/src/main/webapp/images/logo.png");
			logos.add("https://gitlab.com/dpseptiembre/Acme_Santiago/raw/master/src/main/webapp/images/logo.png");

			final String banner = this.randomAds(logos);

			final Image bannerimg = Image.getInstance(new URL(banner).toString());
			bannerimg.scaleToFit(300, 100);

			String chunkContent = "";
			String infoAboutSantiago = "";
			if ("es".equals(locale.getLanguage())) {

				chunkContent = "CERTIFICADO DE COMPOSTELA";
				//FOOTER
				infoAboutSantiago = "Acme, Inc. es una empresa que abarca muchas organizaciones en todo el mundo. Uno de ellos es Acme Santiago,\n" + "NPO, que dirige una red social destinada a ayudar a las personas a peregrinar a Santiago\n"
					+ "de Compostela.";
			} else {
				chunkContent = "COMPOSTELA CERTIFICATE";

				//FOOTER
				infoAboutSantiago = "Acme, Inc. is a holding that encompasses many organisations worldwide. One of them is Acme Santiago,\n" + "NPO, which runs a social network that is intended to help people go on pilgrimages to Santiago\n"
					+ "de Compostela.";
			}

			final Chunk chunk = new Chunk(chunkContent, font);

			document.add(chunk);
			document.add(new Paragraph(" "));
			document.add(new Paragraph("TO / PARA: " + name));
			document.add(new Paragraph("DATE / FECHA: " + date));
			document.add(new Paragraph(userInfo));

			document.add(new Paragraph("WALKS / CAMINATAS"));
			for (final Walk walk1 : walks)
				document.add(new Paragraph(walk1.getName()));

			document.add(new Paragraph("INNS / HOSPEDERĶAS"));
			for (final Hostal in : hostals) {
				final Image image = Image.getInstance((in.getBadge()));
				image.scaleToFit(50, 50);
				document.add(image);
				document.add(new Paragraph(in.getName()));
				document.add(new Paragraph("...."));

			}

			document.add(new Paragraph(" "));

			document.add(new Paragraph(infoAboutSantiago));

			document.add(new Paragraph(" "));
			document.add(new Paragraph(" "));
			document.add(new Paragraph(" ---------------------------------------------------------------------------------------- "));
			document.add(bannerimg);
			document.close();
		} catch (final DocumentException e) {
			e.printStackTrace();
		} catch (final FileNotFoundException e) {
			e.printStackTrace();
		} catch (final IOException e) {
			e.printStackTrace();
		}

	}

	private String randomAds(final List<String> list) {

		try {

			final Random r = new Random();
			final int Low = 0;
			final int High = list.size();
			final int candidate = r.nextInt(High - Low) + Low;
			return list.get(candidate);

		} catch (final Exception e) {
			e.printStackTrace();
			return null;
		}

	}


	public Collection<Hike> getIncompleteHikesGivenWalk(Walk walk){

		Collection<Hike> result = walkRepository.getIncompleteHikesGivenWalk(walk.getId());
		return  result;
	}

	public Collection<Hike> getCompleteHikesGivenWalk(Walk walk){

		Collection<Hike> result = walkRepository.getCompleteHikesGivenWalk(walk.getId());
		return  result;
	}
}
