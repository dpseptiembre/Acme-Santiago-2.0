/*
 * ChirpService.java
 * 
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package services;

import domain.Chirp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import repositories.ChirpRepository;
import security.UserAccountService;

import java.util.Collection;

@Service
@Transactional
public class ChirpService {

	// Managed repository -----------------------------------------------------

	@Autowired
	private ChirpRepository		chirpRepository;

	// Supporting services ----------------------------------------------------



	// Constructors -----------------------------------------------------------

	public ChirpService() {
		super();
	}

	// Simple CRUD methods ----------------------------------------------------

	public Chirp create(){
		Chirp result;
		result = new Chirp();
		return result;
	}
	
	public Collection<Chirp> findAll() {
		Collection<Chirp> result;

		result = chirpRepository.findAll();
		Assert.notNull(result);

		return result;
	}

	public Chirp findOne(int chirpId) {
		Assert.isTrue(chirpId != 0);

		Chirp result;

		result = chirpRepository.findOne(chirpId);
		Assert.notNull(result);

		return result;
	}

	public Chirp save(Chirp chirp) {
		Assert.notNull(chirp);

		Chirp result;

		result = chirpRepository.save(chirp);

		return result;
	}

	public void delete(Chirp chirp) {
		Assert.notNull(chirp);
		Assert.isTrue(chirp.getId() != 0);
		Assert.isTrue(chirpRepository.exists(chirp.getId()));

		chirpRepository.delete(chirp);
	}

	// Other business methods -------------------------------------------------


}
