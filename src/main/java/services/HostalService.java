/*
 * InService.java
 * 
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package services;

import domain.Hike;
import domain.Hostal;
import domain.Innkeeper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import repositories.HostalRepository;

import java.util.Collection;
import java.util.Date;

@Service
@Transactional
public class HostalService {

	// Managed repository -----------------------------------------------------

	@Autowired
	private HostalRepository inRepository;

	// Supporting services ----------------------------------------------------

	// Constructors -----------------------------------------------------------

	public HostalService() {
		super();
	}

	// Simple CRUD methods ----------------------------------------------------

	public Hostal create() {
		Hostal result;
		result = new Hostal();
		return result;
	}

	public Collection<Hostal> findAll() {
		Collection<Hostal> result;

		result = inRepository.findAll();
		Assert.notNull(result);

		return result;
	}

	public Hostal findOne(int inId) {
		Assert.isTrue(inId != 0);

		Hostal result;

		result = inRepository.findOne(inId);
		Assert.notNull(result);

		return result;
	}

	public Hostal save(Hostal hostal) {
		Assert.notNull(hostal);

		Hostal result;

		result = inRepository.save(hostal);

		return result;
	}

	public void delete(Hostal hostal) {
		Assert.notNull(hostal);
		Assert.isTrue(hostal.getId() != 0);
		Assert.isTrue(inRepository.exists(hostal.getId()));

		inRepository.delete(hostal);
	}

	// Other business methods -------------------------------------------------
	public Collection<Hostal> findAllValid() {
		Collection<Hostal> result;
		Date date= new Date();
		int y = date.getYear()+1900;
		int m = date.getMonth()+1;
		result =inRepository.findAllValid(y,m);
		return result;
	}


	public Collection<Hostal> innsByUser(int userId){

	   return inRepository.findInnByUser(userId);

   }


   public Collection<Hike> hikesAwardingDecision(Innkeeper innkeeper){

		return inRepository.hikesAwardingDecision(innkeeper.getId());


   }
}
