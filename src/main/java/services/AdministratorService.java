/*
 * AdministratorService.java
 * 
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package services;

import domain.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import repositories.AdministratorRepository;
import repositories.UtlsRepository;
import security.UserAccount;
import security.UserAccountService;

import java.util.*;

@Service
@Transactional
public class AdministratorService {

	// Managed repository -----------------------------------------------------

	@Autowired
	private AdministratorRepository		administratorRepository;

	// Supporting services ----------------------------------------------------

	@Autowired
	private UserAccountService	userAccountService;
	@Autowired
	private UtlsService utlsService;
	@Autowired
	private ChirpService chirpService;
	@Autowired
	private CommentService commentService;
	@Autowired
	private AdvertisementService advertisementService;
	@Autowired
	private HikeService hikeService;
	@Autowired
	private RouteService routeService;
	@Autowired
	private WalkService walkService;


	// Constructors -----------------------------------------------------------

	public AdministratorService() {
		super();
	}

	// Simple CRUD methods ----------------------------------------------------
	
	
	public Collection<Administrator> findAll() {
		Collection<Administrator> result;

		result = administratorRepository.findAll();
		Assert.notNull(result);

		return result;
	}

	public Administrator findOne(int administratorId) {
		Assert.isTrue(administratorId != 0);

		Administrator result;

		result = administratorRepository.findOne(administratorId);
		Assert.notNull(result);

		return result;
	}

	public Administrator save(Administrator administrator) {
		Assert.notNull(administrator);

		Administrator result;

		result = administratorRepository.save(administrator);

		return result;
	}

	public void delete(Administrator administrator) {
		Assert.notNull(administrator);
		Assert.isTrue(administrator.getId() != 0);
		Assert.isTrue(administratorRepository.exists(administrator.getId()));

		administratorRepository.delete(administrator);
	}

	// Other business methods -------------------------------------------------

//	public UserAccount findUserAccount(Administrator administrator) {
//		Assert.notNull(administrator);
//
//		UserAccount result;
//
//		result = userAccountService.findByAdministrator(administrator);
//
//		return result;
//	}


	public Utls getUtil(){

		try {
			List<Utls> uttils = new ArrayList<>(utlsService.findAll());
			if (uttils.isEmpty()){
				throw new NoSuchElementException("The util list is empty");
			}else {
				Utls first = uttils.get(0);
				return first;
			}

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}



	public void addTabooWord(String word){

		try {
			Utls utls = this.getUtil();
			Assert.notNull(word,"The word is blank");
			if(utls.getTabooWords().contains(word)){
				throw new UnsupportedOperationException("The list already contains the word");

			}else{
				utls.getTabooWords().add(word);
				utlsService.save(utls);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}


	public void removeTabooWord(String word){

		try {
			Utls utls = this.getUtil();
			Assert.notNull(word,"The word is blank");
			if(!utls.getTabooWords().contains(word)){
				throw new UnsupportedOperationException("The list doesn't contains the word");

			}else{
				utls.getTabooWords().remove(word);
				utlsService.save(utls);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}


	public Collection<Chirp> getChirpWithTabooWords(){
		List<Chirp> res = new ArrayList<>();
		try {
			Utls utls = this.getUtil();

			for(String c: utls.getTabooWords()){
				res.addAll(administratorRepository.spamChirps(c));
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return res;
	}

	public Collection<Comment> getCommentsWithTabooWords(){
		List<Comment> res = new ArrayList<>();
		try {
			Utls utls = this.getUtil();

			for(String c: utls.getTabooWords()){
				res.addAll(administratorRepository.spamComments(c));
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return res;
	}

	public Collection<Advertisement> getAdvertisementWithTabooWords(){
		List<Advertisement> res = new ArrayList<>();
		try {
			Utls utls = this.getUtil();

			for(String c: utls.getTabooWords()){
				res.addAll(administratorRepository.spamAdvertisement(c));
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return res;
	}


	public void removeChirp(int chirpId){

		try {
			Chirp chirp = chirpService.findOne(chirpId);
			Assert.notNull(chirp);
			chirp.setHidden(true);
			chirpService.save(chirp);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}


	public void removeComment(int commentId){

		try {
			Comment comment = commentService.findOne(commentId);
			Assert.notNull(comment);
			comment.setHidden(true);
			commentService.save(comment);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}


	public void removeAdvertisement(int advertisementId){

		try {
			Advertisement advertisement = advertisementService.findOne(advertisementId);
			Assert.notNull(advertisement);
			advertisement.setHidden(true);
			advertisementService.save(advertisement);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}


	public Collection<Walk> walksWithCompostelasAwardingDecision(){
		List<Walk> walks = new ArrayList<>();

		try {
			walks.addAll(administratorRepository.walksAwardingDecision());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return walks;
	}



	public void requestDecisionAboutCompostela(int walkId){


		try {
			Walk walk = walkService.findOne(walkId);
			if(!administratorRepository.getHikesWithoutDestinationInGivenWalk(walk.getId()).isEmpty()){
				walk.setSugestedStatus(Status.DENY);
			}else {
				walk.setSugestedStatus(Status.ACCEPTED);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	public void approveCompostela(int walkId){

		try {
			Walk walk = walkService.findOne(walkId);
			walk.setCompostelaStatus(Status.ACCEPTED);
			walkService.save(walk);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void denyCompostela(String walkId, String reason){

		try {
			Integer walkIDNum = new Integer(walkId);
			Walk walk = walkService.findOne(walkIDNum);
			walk.setCompostelaStatus(Status.DENY);
			walk.setDenyReason(reason);
			walkService.save(walk);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	public void removeRoute(int routeId){

		try {
			Route route = routeService.findOne(routeId);
			List<Hike> hikes = new ArrayList<>(route.getHikes());
			for (Hike hike:hikes){
				hike.setHidden(true);
				hikeService.save(hike);
			}
			route.setHidden(true);
			routeService.save(route);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}



	//DASHBOARD

	public double averageRouterPerUser(){
		return administratorRepository.averageRouterPerUser();
	}
	public double standarDesviationOfRoutesPerUser(){
		return administratorRepository.standarDesviationOfRoutesPerUser();
	}

	public double averageOfHikesPerRoute(){
		return administratorRepository.averageOfHikesPerRoute();
	}
	public double standarDesviationOfHikesPerRoute(){
		return administratorRepository.standarDesviationOfHikesPerRoute();
	}

	public double averageOfRoutesLenght(){
		return administratorRepository.averageOfRoutesLenght();
	}
	public double standarDesviationOfRoutesLenght(){
		return administratorRepository.standarDesviationOfRoutesLenght();
	}

	public double averageOfHikesLenght(){
		return administratorRepository.averageOfHikesLenght();
	}
	public double standarDesviationOfHikesLenght(){
		return administratorRepository.standarDesviationOfHikesLenght();
	}

	public double averageNumberOfChirpsPerUser(){
		return administratorRepository.averageNumberOfChirpsPerUser();
	}
	public double averageNumberOfCommentsPerRouteIncludingHikes(){
		return administratorRepository.averageNumberOfCommentsPerRouteIncludingHikes();
	}

	public double averageNumberOfInnsManagerPerKeepeer(){
		return administratorRepository.averageNumberOfInnsManagerPerKeepeer();
	}
	public double standarDesviationOfInnsManagedPerKeeper(){
		return administratorRepository.standarDesviationOfInnsManagedPerKeeper();
	}

	public double standarDesviationOfUserRegistrationPerDay(){
		return administratorRepository.standarDesviationOfUserRegistrationPerDay();
	}

	public double avgNumberOfuserRegistrationsPerDay(){
		return administratorRepository.avgNumberOfuserRegistrationsPerDay();
	}

	public Collection<User> userWhoHasPostedLessThan25AverageNumberOfChirpsPerUser(){
		return administratorRepository.userWhoHasPostedLessThan25AverageNumberOfChirpsPerUser();
	}
	public Collection<User> userWhoHasPostedMoreThan75AverageNumberOfChirpsPerUser(){
		return administratorRepository.userWhoHasPostedMoreThan75AverageNumberOfChirpsPerUser();
	}

	public Integer ratioOfRoutesWithAdvertisementVersusNot(){

		Integer numOfRoutesWithAdvertisment = administratorRepository.routesWithAdvertismenet().size();
		Integer numOfRoutesWithoutAdvertisment = administratorRepository.routesWithoutAdvertisement().size();

		if (numOfRoutesWithoutAdvertisment == 0){
			return numOfRoutesWithAdvertisment;
		}else {
			return numOfRoutesWithAdvertisment/numOfRoutesWithoutAdvertisment;
		}




	}

	public Integer ratioOfAdvertismentWithTabooWords(){

		Integer numOfAdvertismentWithTabooWords = this.getAdvertisementWithTabooWords().size();
		Integer numbOfAdvertisement = administratorRepository.notSpamAdvertisement().size();

		if (numOfAdvertismentWithTabooWords == 0){
			return numbOfAdvertisement;
		}else {
			return numbOfAdvertisement/numOfAdvertismentWithTabooWords;
		}



	}

	public Integer ratioOfCompostelasWalking(){

		Integer numWalksAwardingDecision = administratorRepository.walksAwardingDecision().size();
		Integer numComposteladDecided = administratorRepository.allCompostelas().size();

		if (numWalksAwardingDecision == 0){
			return numComposteladDecided;
		}else {
			return numComposteladDecided/numWalksAwardingDecision;
		}



	}

	public Integer ratioOfCompostelasRequestedVsCompostelaRejected(){

		Integer numCompostelasAccepted = administratorRepository.compostelasAccepted().size();
		Integer numCompostelasDeclined = administratorRepository.compostelasDeclined().size();


		if (numCompostelasDeclined == 0){
			return numCompostelasAccepted;
		}else {
			return numCompostelasAccepted/numCompostelasDeclined;
		}


	}

	//Dashboard -- Outliner Section

	public Collection<Route> outliner(){
		double avg = administratorRepository.averageOfRoutesLenght();
		double std = administratorRepository.standarDesviationOfRoutesLenght();
		Collection<Route> outliersRoutesRes = new HashSet<>();
	   for (Route route : routeService.findAll()){
	      if (route.getLgt() >= -(avg-3*std) || route.getLgt() >= avg +3*std){
	         outliersRoutesRes.add(route);
         }
      }



	   return outliersRoutesRes;
	}

}
