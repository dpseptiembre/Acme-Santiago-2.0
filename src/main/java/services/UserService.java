/*
 * UserService.java
 * 
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package services;

import domain.Actor;
import domain.Route;
import domain.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import repositories.UserRepository;
import security.Authority;
import security.LoginService;
import security.UserAccount;
import security.UserAccountService;

import java.util.ArrayList;
import java.util.Collection;

@Service
@Transactional
public class UserService {

	// Managed repository -----------------------------------------------------

	@Autowired
	private UserRepository		userRepository;

	// Supporting services ----------------------------------------------------

	@Autowired
	private UserAccountService	userAccountService;


	// Constructors -----------------------------------------------------------

	public UserService() {
		super();
	}

	// Simple CRUD methods ----------------------------------------------------
	

	public User create() {
		final User res = new User();
		this.authoritySet(res);
		return res;

	}
	
	public Collection<User> findAll() {
		Collection<User> result;

		result = userRepository.findAll();
		Assert.notNull(result);

		return result;
	}

	public User findOne(int userId) {
		Assert.isTrue(userId != 0);

		User result;

		result = userRepository.findOne(userId);
		Assert.notNull(result);

		return result;
	}

	public User save(User user) {
		Assert.notNull(user);

		User result;
		

		result=userRepository.save(user);
		return result;
	}

	public void delete(User user) {
		Assert.notNull(user);
		Assert.isTrue(user.getId() != 0);
		Assert.isTrue(userRepository.exists(user.getId()));

		userRepository.delete(user);
	}

	// Other business methods -------------------------------------------------

//	public UserAccount findUserAccount(User user) {
//		Assert.notNull(user);
//
//		UserAccount result;
//
//		result = userAccountService.findByUser(user);
//
//		return result;
//	}
	
	public UserAccount findUserAccount(final User user) {
		Assert.notNull(user);

		UserAccount result;

		result = this.userAccountService.findByUser(user);

		return result;
	}

	public User findByPrincipal() {
		User result;
		UserAccount userAccount;
		userAccount = LoginService.getPrincipal();
		Assert.notNull(userAccount);
		result = this.findByUserAccount(userAccount);
		Assert.notNull(result);
		return result;
	}

	public User findByUserAccount(final UserAccount userAccount) {
		Assert.notNull(userAccount);

		User result;

		result = this.userRepository.findByUserAccountId(userAccount.getId());

		return result;
	}

	public Actor registerAsUser(final User u) {
		Assert.notNull(u);
		u.getUserAccount().setUsername(u.getUserAccount().getUsername());
		Md5PasswordEncoder encoder;
		encoder = new Md5PasswordEncoder();
		final String hash = encoder.encodePassword(u.getUserAccount().getPassword(), null);
		u.getUserAccount().setPassword(hash);
		final UserAccount userAccount = this.userAccountService.save(u.getUserAccount());
		u.setUserAccount(userAccount);
		Assert.notNull(u.getUserAccount().getAuthorities(), "authorities null al registrar");
		final User resu = this.userRepository.save(u);
		return resu;
	}

	private void authoritySet(final User user) {
		Assert.notNull(user);
		final Authority autoh = new Authority();
		autoh.setAuthority(Authority.USER);
		final Collection<Authority> authorities = new ArrayList<>();
		authorities.add(autoh);
		final UserAccount res1 = new UserAccount();
		res1.setAuthorities(authorities);
		//final UserAccount userAccount = this.userAccountService.save(res1);
		user.setUserAccount(res1);
		Assert.notNull(user.getUserAccount().getAuthorities(), "authorities null al registrar");
	}
}
