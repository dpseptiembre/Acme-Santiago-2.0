/*
 * CreditCardService.java
 * 
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package services;

import domain.CreditCard;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import repositories.CreditCardRepository;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;

@Service
@Transactional
public class CreditCardService {

	// Managed repository -----------------------------------------------------

	@Autowired
	private CreditCardRepository		creditCardRepository;

	// Supporting services ----------------------------------------------------
	


	// Constructors -----------------------------------------------------------

	public CreditCardService() {
		super();
	}

	// Simple CRUD methods ----------------------------------------------------

	public CreditCard create(){
		CreditCard result;
		result = new CreditCard();
		return result;
	}
	
	public Collection<CreditCard> findAll() {
		Collection<CreditCard> result;

		result = creditCardRepository.findAll();
		Assert.notNull(result);

		return result;
	}

	public CreditCard findOne(int creditCardId) {
		Assert.isTrue(creditCardId != 0);

		CreditCard result;

		result = creditCardRepository.findOne(creditCardId);
		Assert.notNull(result);

		return result;
	}

	public CreditCard save(CreditCard creditCard) {
		Assert.notNull(creditCard);

		CreditCard result;

		result = creditCardRepository.save(creditCard);

		return result;
	}

	public void delete(CreditCard creditCard) {
		Assert.notNull(creditCard);
		Assert.isTrue(creditCard.getId() != 0);
		Assert.isTrue(creditCardRepository.exists(creditCard.getId()));

		creditCardRepository.delete(creditCard);
	}

	// Other business methods -------------------------------------------------


	public Boolean checkCreditCard2(CreditCard creditCard) {
		Boolean res = false;

		try {
			String year = creditCard.getYear().toString();
			String month = creditCard.getMonth().toString();

			SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-MM");
			Date creditCardDate = myFormat.parse(year+"-"+month);

			Date now = new Date(System.currentTimeMillis()-1000);
			Date threeMonthsNext = new DateTime().plusMonths(3).toDate();

			if (creditCardDate.before(now)){
				res = false;
			}else res = creditCardDate.after(threeMonthsNext);


		} catch (ParseException e) {
			e.printStackTrace();
		}

		return res;
	}

}
