
package services;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import security.UserAccount;
import security.UserAccountService;
import utilities.AbstractTest;
import domain.Agent;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:spring/datasource.xml", "classpath:spring/config/packages.xml"
})
@Transactional
public class AgentServiceTest extends AbstractTest {

	@Autowired
	private AgentService		agentService;
	@Autowired
	private UserAccountService	userAccountService;


	@Test
	public void registerAsAgent() {
		final Agent u = this.agentService.create();
		Assert.notNull(u);
		u.getUserAccount().setUsername("kkkkkk");
		u.setName("kkkkkk");
		u.setSurname("sdfsdf");
		u.setEmail("perri@gjail.com");
		Md5PasswordEncoder encoder;
		encoder = new Md5PasswordEncoder();
		final String hash = encoder.encodePassword("kkkkkk", null);
		u.getUserAccount().setPassword(hash);
		org.junit.Assert.assertNotNull(u);
	}

	@Test(expected = AssertionError.class)
	public void registerAsAgentNegative() {
		final Agent u = this.agentService.create();
		u.setName("perrito");
		u.getUserAccount().setUsername("perrito");
		u.getUserAccount().setPassword("perrito");
		Assert.notNull(u);
		u.getUserAccount().setUsername(u.getUserAccount().getUsername());
		Md5PasswordEncoder encoder;
		encoder = new Md5PasswordEncoder();
		final String hash = encoder.encodePassword(u.getUserAccount().getPassword(), null);
		u.getUserAccount().setPassword(hash);
		final UserAccount userAccount = this.userAccountService.save(u.getUserAccount());
		u.setUserAccount(userAccount);
		Assert.notNull(u.getUserAccount().getAuthorities(), "authorities null al registrar");
		final Agent resu = this.agentService.save(u);
		org.junit.Assert.assertNotNull(resu.getEmail());
	}

}
