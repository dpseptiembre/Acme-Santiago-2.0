
package services;

import java.util.List;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import utilities.AbstractTest;
import domain.Advertisement;
import domain.Agent;
import domain.Hike;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:spring/datasource.xml", "classpath:spring/config/packages.xml"
})
@Transactional
public class AdvertisementServiceTest extends AbstractTest {

	@Autowired
	private AdvertisementService	advertisementService;
	@Autowired
	private AgentService			agentService;
	@Autowired
	private HikeService				hikeService;


	@Test
	public void createAdvertisement() {
		this.authenticate("agent1");
		final Advertisement a = this.advertisementService.create();
		final List<Hike> hikes = (List<Hike>) this.hikeService.findAll();
		a.setBanner("ban");
		a.setCreator(this.agentService.findByPrincipal());
		a.setDisplayDays(3);
		a.setTitle("Titulo");
		a.setHike(hikes.get(0));
		Assert.notNull(a);
		this.unauthenticate();
	}

	@Test(expected = IllegalArgumentException.class)
	public void createAdvertisementNegative() {
		this.authenticate("agent1");
		final Advertisement a = this.advertisementService.create();
		final List<Hike> hikes = (List<Hike>) this.hikeService.findAll();
		a.setBanner("ban");
		a.setCreator(this.agentService.findByPrincipal());
		a.setDisplayDays(3);
		a.setTitle("Titulo");
		a.setHike(hikes.get(0));
		Assert.notNull(a.getRoutes());
		this.unauthenticate();
	}

	@Test
	public void adsAgent() {
		this.authenticate("agent1");
		final Agent a = this.agentService.findByPrincipal();
		Assert.notNull(a.getAdvertisements());
		this.unauthenticate();
	}

	@Test(expected = IllegalArgumentException.class)
	public void adsAgentNegative() {
		final Agent a = this.agentService.findByPrincipal();
		Assert.notNull(a.getAdvertisements());
	}

	@Test
	public void noAdsAgent() {
		this.authenticate("agent1");
		final Agent a = this.agentService.findByPrincipal();
		final List<Advertisement> ads = (List<Advertisement>) this.advertisementService.findAll();
		ads.removeAll(a.getAdvertisements());
		Assert.notNull(ads);
		this.unauthenticate();
	}

	@Test(expected = IllegalArgumentException.class)
	public void noAdsAgentNegative() {
		final Agent a = this.agentService.findByPrincipal();
		final List<Advertisement> ads = (List<Advertisement>) this.advertisementService.findAll();
		ads.removeAll(a.getAdvertisements());
		Assert.notNull(ads);
	}

}
