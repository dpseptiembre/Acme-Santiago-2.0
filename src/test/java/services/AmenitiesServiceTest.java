
package services;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import utilities.AbstractTest;
import domain.Amenities;
import domain.Hostal;
import domain.Innkeeper;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:spring/datasource.xml", "classpath:spring/config/packages.xml"
})
@Transactional
public class AmenitiesServiceTest extends AbstractTest {

	@Autowired
	private AmenitiesService	amenitiesService;
	@Autowired
	private InnkeeperService	innkeeperService;
	@Autowired
	private HostalService		inService;


	@Test
	public void listAmenities() {
		final List<Amenities> ams = (List<Amenities>) this.amenitiesService.findAll();
		Assert.assertNotNull(ams);
	}

	@Test
	public void createAmenities() {
		this.authenticate("innkeeper1");
		final Amenities a = this.amenitiesService.create();
		a.setDescription("Desc");
		a.setName("Nombre");
		Assert.assertNotNull(a);
	}

	@Test(expected = AssertionError.class)
	public void createAmenitiesNegative() {
		this.authenticate("innkeeper1");
		final Amenities a = this.amenitiesService.create();
		a.setDescription("Desc");
		a.setName("Nombre");
		Assert.assertNotNull(a.getHostal());
	}

	@Test
	public void editAmenitie() {
		this.authenticate("innkeeper1");
		final Innkeeper innkeeper = this.innkeeperService.findByPrincipal();
		final Hostal hostal = new ArrayList<>(innkeeper.getHostals()).get(0);
		final Amenities amenities = new ArrayList<>(hostal.getAmenities()).get(0);
		final String name = amenities.getName();
		amenities.setName("perri");
		final Amenities amenities1 = this.amenitiesService.save(amenities);
		Assert.assertNotEquals(name, amenities1.getName());
		Assert.assertNotNull(amenities1.getHostal());
	}

	@Test
	public void removeAd() {
		this.authenticate("innkeeper1");
		final List<Amenities> ads = (List<Amenities>) this.amenitiesService.findAll();
		final Integer initialSize = ads.size();
		ads.remove(0);
		final Integer finalSize = ads.size();
		org.junit.Assert.assertNotEquals(initialSize, finalSize);
		this.unauthenticate();
	}

}
