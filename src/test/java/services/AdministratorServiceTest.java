
package services;

import java.util.Collection;
import java.util.List;

import javax.transaction.Transactional;

import junit.framework.Assert;
import junit.framework.AssertionFailedError;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import utilities.AbstractTest;
import domain.Advertisement;
import domain.Status;
import domain.Walk;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:spring/datasource.xml", "classpath:spring/config/packages.xml"
})
@Transactional
public class AdministratorServiceTest extends AbstractTest {

	@Autowired
	private AdministratorService	administratorService;


	@Test
	public void tabooAds() {
		this.authenticate("administrator1");
		final List<Advertisement> ads = (List<Advertisement>) this.administratorService.getAdvertisementWithTabooWords();
		Assert.assertNotNull(ads);
		this.unauthenticate();
	}

	@Test(expected = IndexOutOfBoundsException.class)
	public void tabooAdsNegative() {
		this.authenticate("administrator1");
		final List<Advertisement> ads = (List<Advertisement>) this.administratorService.getAdvertisementWithTabooWords();
		Assert.assertNotNull(ads.get(80));
		this.unauthenticate();
	}

	@Test
	public void removeAd() {
		this.authenticate("administrator1");
		final List<Advertisement> ads = (List<Advertisement>) this.administratorService.getAdvertisementWithTabooWords();
		final Integer initialSize = ads.size();
		ads.remove(0);
		final Integer finalSize = ads.size();
		org.junit.Assert.assertNotEquals(initialSize, finalSize);
		this.unauthenticate();
	}

	@Test
	public void listRequest() {
		this.authenticate("administrator1");
		final Collection<Walk> walks = this.administratorService.walksWithCompostelasAwardingDecision();
		Assert.assertNotNull(walks);
		this.unauthenticate();
	}

	@Test(expected = IndexOutOfBoundsException.class)
	public void listRequestNegative() {
		this.authenticate("administrator1");
		final List<Walk> walks = (List<Walk>) this.administratorService.walksWithCompostelasAwardingDecision();
		Assert.assertNotNull(walks.get(80));
		this.unauthenticate();
	}

	@Test
	public void decideRequest() {
		this.authenticate("administrator1");
		final List<Walk> walks = (List<Walk>) this.administratorService.walksWithCompostelasAwardingDecision();
		final Walk w = walks.get(0);
		w.setCompostelaStatus(Status.ACCEPTED);
		Assert.assertEquals(Status.ACCEPTED, w.getCompostelaStatus());
		this.unauthenticate();
	}

	@Test(expected = AssertionFailedError.class)
	public void decideRequestNegative() {
		this.authenticate("administrator1");
		final List<Walk> walks = (List<Walk>) this.administratorService.walksWithCompostelasAwardingDecision();
		final Walk w = walks.get(0);
		w.setCompostelaStatus(Status.DENY);
		Assert.assertEquals(Status.ACCEPTED, w.getCompostelaStatus());
		this.unauthenticate();
	}

}
