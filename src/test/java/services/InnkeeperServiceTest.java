
package services;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import utilities.AbstractTest;
import domain.Hostal;
import domain.Innkeeper;
import domain.Registration;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:spring/datasource.xml", "classpath:spring/config/packages.xml"
})
@Transactional
public class InnkeeperServiceTest extends AbstractTest {

	@Autowired
	private InnkeeperService	innkeeperService;
	@Autowired
	private AmenitiesService	amenitiesService;


	@Test
	public void certifyUser() {
		this.authenticate("innkeeper1");
		Innkeeper innkeeper;
		innkeeper = this.innkeeperService.findByPrincipal();
		final List<Hostal> hostals = (List<Hostal>) this.amenitiesService.findByInnkeeperId(innkeeper.getId());
		final List<Registration> registrations = new ArrayList<Registration>();
		final List<Registration> res = new ArrayList<Registration>();
		for (final Hostal hostal : hostals) {
			registrations.addAll(hostal.getRegistrations());
			for (final Registration reg : registrations)
				if (reg.getStartDate() != null)
					res.add(reg);
			registrations.removeAll(hostal.getRegistrations());
		}
		Assert.notNull(registrations);
		this.unauthenticate();
	}

	@Test(expected = IndexOutOfBoundsException.class)
	public void certifyUserNegative() {
		this.authenticate("innkeeper1");
		Innkeeper innkeeper;
		innkeeper = this.innkeeperService.findByPrincipal();
		final List<Hostal> hostals = (List<Hostal>) this.amenitiesService.findByInnkeeperId(innkeeper.getId());
		final List<Registration> registrations = new ArrayList<Registration>();
		final List<Registration> res = new ArrayList<Registration>();
		for (final Hostal hostal : hostals) {
			registrations.addAll(hostal.getRegistrations());
			for (final Registration reg : registrations)
				if (reg.getStartDate() != null)
					res.add(reg);
			registrations.removeAll(hostal.getRegistrations());
		}
		Assert.notNull(registrations.get(80));
		this.unauthenticate();
	}

}
