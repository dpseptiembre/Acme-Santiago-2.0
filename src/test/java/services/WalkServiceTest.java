
package services;

import java.util.List;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import utilities.AbstractTest;
import domain.Route;
import domain.Status;
import domain.User;
import domain.Walk;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:spring/datasource.xml", "classpath:spring/config/packages.xml"
})
@Transactional
public class WalkServiceTest extends AbstractTest {

	@Autowired
	private UserService	userService;
	@Autowired
	private WalkService	walkService;


	@Test
	public void createWalk() {
		this.authenticate("user1");
		final Walk w = this.walkService.create();
		final User u = this.userService.findByPrincipal();
		final List<Route> routes = (List<Route>) u.getRoutes();
		final Route r = routes.get(0);
		w.setAuthor(u);
		w.setName("Nombre");
		Assert.notNull(w);
		this.unauthenticate();
	}

	@Test(expected = IllegalArgumentException.class)
	public void createWalkNegative() {
		this.authenticate("user1");
		final Walk w = this.walkService.create();
		final User u = this.userService.findByPrincipal();
		final List<Route> routes = (List<Route>) u.getRoutes();
		final Route r = routes.get(0);
		w.setAuthor(u);
		w.setName("Nombre");
		Assert.notNull(w.getDenyReason());
		this.unauthenticate();
	}

	@Test
	public void listWalk() {
		this.authenticate("user1");
		final User u = this.userService.findByPrincipal();
		Assert.notNull(u.getWalks());
		this.unauthenticate();
	}

	@Test(expected = IndexOutOfBoundsException.class)
	public void listWalkNegative() {
		this.authenticate("user1");
		final User u = this.userService.findByPrincipal();
		final List<Walk> walks = (List<Walk>) u.getWalks();
		Assert.notNull(walks.get(80));
		this.unauthenticate();
	}

	@Test
	public void requestWalk() {
		this.authenticate("user1");
		final User u = this.userService.findByPrincipal();
		final List<Walk> walks = (List<Walk>) u.getWalks();
		final Walk w = walks.get(0);
		w.setCompostelaStatus(Status.PENDING);
		Assert.notNull(w);
		this.unauthenticate();
	}

	@Test(expected = IndexOutOfBoundsException.class)
	public void requestWalkNegative() {
		this.authenticate("user1");
		final User u = this.userService.findByPrincipal();
		final List<Walk> walks = (List<Walk>) u.getWalks();
		final Walk w = walks.get(80);
		w.setCompostelaStatus(Status.PENDING);
		Assert.notNull(w);
		this.unauthenticate();
	}

}
