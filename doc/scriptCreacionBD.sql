DROP DATABASE IF EXISTS `Acme-Santiago-2.0`;
CREATE DATABASE `Acme-Santiago-2.0`;
DROP USER 'acme-user'@'%';
DROP USER 'acme-manager'@'%';
CREATE USER 'acme-user'@'%'
  IDENTIFIED BY PASSWORD '*4F10007AADA9EE3DBB2CC36575DFC6F4FDE27577';
CREATE USER 'acme-manager'@'%'
  IDENTIFIED BY PASSWORD '*FDB8CD304EB2317D10C95D797A4BD7492560F55F';
GRANT SELECT, INSERT, UPDATE, DELETE
ON `Acme-Santiago-2.0`.* TO 'acme-user'@'%';

GRANT SELECT, INSERT, UPDATE, DELETE, CREATE, DROP, REFERENCES, INDEX, ALTER,
CREATE TEMPORARY TABLES, LOCK TABLES, CREATE VIEW, CREATE ROUTINE,
ALTER ROUTINE, EXECUTE, TRIGGER, SHOW VIEW
ON `Acme-Santiago-2.0`.* TO 'acme-manager'@'%';

